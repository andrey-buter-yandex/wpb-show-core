jQuery(document).ready(function($) {

	// JPlayer instance is set by player.php module
	if (typeof JPlayer === 'object') {

		// No need to check upon playlists (done by player.php)
		// audio is array of audio objects (playlist for current player)
		$(JPlayer.playlists).each(function(num, audio){

			// There is player in html for each plalist
			var player = $('#player-'+num),
				container = player.parents('.player-container'),
				loader = container.find( '.loading' ),
				playIndex = 0,
				hasPreroll = false;

			// Player ui elements
			var currentTitle = container.find('.jp-current-title');

			// Initialize player
			/*
			var playerAdapter = new jPlayerAndroidFix(
				player,
				audio,
				{
					*/

			var status = player.jPlayer(
				{
					ready: function( event ) {

						// Set initial player media
						playlistConfig(playIndex);

						// Loader is showing only while page loads
						loader.remove();

						// Check if current playlist has preroll inside
						// Preroll is allways singlular and first
						if ( isPreroll() ) {
							hasPreroll = true;
						}

						// Check autoplay feature
						if (player.attr('data-autoplay') === '1') {
							console.log(player.attr('data-autoplay'));
							player.jPlayer('play');
						}

						// When volume is disabled there need to be some css changes done
						if ( event.jPlayer.status.noVolume ) {
							container.addClass('no-volume');
						}
					},
					play: function() { 

						// Avoid both jPlayers playing together.
						player.jPlayer("pauseOthers");

						setTitle();
						collectStats();
					},
					oggSupport: false,
					swfPath: JPlayer.swfPath,
					supplied: "mp3",
					wmode: "window",
					cssSelectorAncestor: "#player-interface-"+num
				}
			);

			//console.dir( status );

			/**
			 * Setup player media according to passed index
			 * @param  {int} index playlist index
			 * @return {[type]}       [description]
			 */
			function playlistConfig(index) {

				// PlayerAdapter.setMedia( audio[index] );
				player.jPlayer("setMedia", audio[index]);
			}

			/**
			 * Set current playing title
			 */
			function setTitle() {
				currentTitle.html( audio[playIndex].title );
			}

			// Create an ended event handler to move to the next item
			player.on($.jPlayer.event.ended, function(event) {
				playListNext();
			});

			/**
			 * Play next playlist item
			 * @return {void}
			 */
			function playListNext() {
				var index = (playIndex+1 < audio.length) ? playIndex+1 : 0;
				playListChange( index );

				// There is no repeat, and player is minimized
				if (index == 0) {
					minimizePlayer();
				} else {
					player.jPlayer('play');
				}
			}

			/**
			 * Set audio according to passed index
			 * @param  {int} index playlist index
			 * @return {void}
			 */
			function playListChange( index ) {
				playIndex = index;
				playlistConfig( index );
			}

			/**
			 * Check if playlist item is preroll
			 * @param  {int}  index playlist item to check
			 * @return {bool}
			 */
			function isPreroll( index ) {
				if ( typeof index == 'number' ) {
					return audio[index].hasOwnProperty('preroll');
				} else {
					return audio[playIndex].hasOwnProperty('preroll');
				}
			}

			/**
			 * Toggle player condition classes on player container
			 */
			player
				.on($.jPlayer.event.play, function(event) {
					container
						.addClass('playing')
						.removeClass('pause');

					// Handle Preroll classes
					if ( isPreroll() ) {
						container.addClass('playing-preroll');
					} else {
						container.removeClass('playing-preroll');
					}
						
				})
				.on($.jPlayer.event.pause, function(event) {
					container
						.addClass('pause')
						.removeClass('playing');
				});

			/**
			 * Collect player statistics
			 * Stats is collected for 
			 * @return {void}
			 */
			function collectStats() {
				var type,
					action = 'player_played',
					data = {};

				if (!audio[playIndex].hasOwnProperty('played')) {
					type = (isPreroll()) ? 'preroll' : 'audio';

					// Prepare data to send with notification request
					data.action = action;
					data.type   = type;
					data.id     = audio[playIndex].id;

					// Send preroll id if audio is playing and current plalist contains preroll
					if ( !isPreroll() && hasPreroll ) {
						data.preroll_id = audio[0].id;
					}

					// We are not waiting for any data response
					$.ajax({
						type: 'GET',
						url:  ajaxurl,
						data: data
					});

					audio[playIndex].played = 1;
				}
			}

		}) // foreach playlists loop
	} // instance presence check

});