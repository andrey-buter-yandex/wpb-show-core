<?php
/**
* jQuery Jplayer template
* @uses object global player instance
*/
global $player;
?>
<div class="player-container pause">
	<img class="loading" src="<?php echo $player->http_path . $player->settings['skin'] ?>ajax-loader.gif" alt="">
	<div <?php $player->render_attributes(); ?> id="player-<?php $player->the_instance() ?>" class="jp-jplayer"></div>
	<div id="player-interface-<?php $player->the_instance() ?>" class="jp-audio">
		<div class="jp-type-playlist">
			<div class="jp-gui jp-interface">
				<ul class="jp-controls">
					<li><a class="jp-play" href="javascript:;" style="display: block;">play</a></li>
					<li><a class="jp-pause" href="javascript:;" style="display: none;">pause</a></li>
				</ul>
				<div class="jp-progress" style="display: block;"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
				<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
				<div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div></div>
				<div class="jp-current-title"></div>
			</div>
			<div class="jp-no-solution">To play the media you will need to either update your browser to a recent version or update your Flash plugin. - http://get.adobe.com/flashplayer/</div>
		</div>
	</div>
</div>