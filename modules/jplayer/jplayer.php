<?php
/**
* jQuery Jplayer module
*/

/* Public functions */

function player( $audio = array() ) {
	echo get_player( $audio );
}

/**
 * Get player html
 * @param  array  $audio Audio files
 * @return string        Html containning player
 */
function get_player( $audio = array(), $options = array() ) {
	global $player;

	return $player->get_player( $audio, $options );
}


/**
 * Initialize jplayer module class
 */
class JPlayer {

	/**
	 * Hold module http path
	 * @var string
	 */
	var $http_path;

	/**
	 * Hold mudule settings
	 * @var array
	 */
	var $settings = array(
		'version' => '0.01',
		'skin'    => '/skin/blue.monday.minimal/'
		);

	/**
	 * Hold all playlists on current page
	 * @var array
	 */
	var $playlists = array();


	function __construct() {

		// Set module variables
		$this->http_path = WPBC_MODULE_URI .'/'. basename( __DIR__ );

		// Include necessary scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );

		// Add js object
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );
	}

	/**
	 * Include necessary scripts and styles
	 * @return void
	 */
	function enqueue_assets() {

		wp_register_script( 'jquery.jplayer', $this->http_path . '/jquery.jplayer.min.js', array( 'jquery' ), '2.4.0', true );
		wp_enqueue_script( 'jquery.jplayer' );

		// Android fix
		wp_register_script( 'jquery.jplayer.android', $this->http_path . '/jquery.android-adapter-fix.js', array( 'jquery.jplayer' ), '2.3', true );
		wp_enqueue_script( 'jquery.jplayer.android' );

		// module core js
		wp_register_script( 'jquery.jplayer-init', $this->http_path . '/jquery.jplayer-init.js', array( 'jquery' ), '2.4.0', true );
		wp_enqueue_script( 'jquery.jplayer-init' );

		wp_register_style('jplayer.blue.monday', $this->http_path . '/skin/blue.monday.minimal/jplayer.blue.monday.css' , false, '2', false );
		wp_enqueue_style('jplayer.blue.monday');
	}

	/**
	 * Get player html and attached passed audio files to it
	 * @param  array  $audio array with audio files
	 * @return string        html player
	 */
	function get_player( $audio = array(), $options = array() ) {
		$this->playlists[] = $audio;

		// Per player options
		$this->attributes = $options;

		ob_start();
		require( 'template/player.php' );
		return ob_get_clean();
	}

	/**
	 * Render additional player attributes
	 * @return void
	 */
	function render_attributes() {
		if ( !empty( $this->attributes ) && is_array( $this->attributes ) ) {
			foreach ( $this->attributes as $key => $value ) {
				echo ' data-'.$key.'="'.$value.'"';
			}
		}
	}

	/**
	 * OUtput current player instance
	 * @return void
	 */
	function the_instance() {
		echo $this->get_instance();
	}

	/**
	 * Get current player instance according to the number of playlists
	 * @return int instance
	 */
	function get_instance() {
		return count( $this->playlists ) - 1;
	}

	/**
	 * Pass necessary js parametrs to player instance
	 * @return void
	 */
	function wp_footer() {

		wp_localize_script( 'jquery.jplayer-init', 'JPlayer', array(
			'swfPath'   => $this->http_path . '/assets/',
			'playlists' => $this->playlists
		));
	}
}

// Initialize player
$GLOBALS['player'] = new JPlayer;