<?php

/**
* Allow users to select active stream for 
* Adds admin interface for viewing stats
*
* NOTICE we assume that all audio on site are episodes (and prerolls)
*/
class ITMWP_Show_Stream 
{
	/**
	 * Hold module abspath
	 * @var string
	 */
	var $include_path;

	function __construct() {
		$this->include_path = WPBC_MODULE_ABS . '/'. basename( __DIR__ );

		$this->load_partials();

		add_action( 'widgets_init', array( $this, 'init_show_stream_widget' ) );

		// Apply user choise of active stream for the front-end only
		if ( !is_admin() )
			add_action( 'init', array( $this, 'check_active_stream' ) );
	}

	/**
	 * Load dependany partials
	 * @return void
	 */
	public function load_partials() 
	{
		require_once( $this->include_path . '/widget.php' );
	}

	/**
	 * Initialize widget 
	 * loaded as module partial
	 * @return void
	 */
	public function init_show_stream_widget() 
	{
		register_widget( 'ITMWPB_Show_Stream_Widget' );
	}

	/**
	 * Set active stream for user either from cookies or from GET parametr
	 * @return void
	 */
	public function check_active_stream() 
	{
		// Update active stream if necessary
		$stream = $this->update_active_stream();

		if ( empty( $stream ) )
			return;

		set_show_stream( $stream );
	}

	/**
	 * Maybe update active show stream on demand
	 * @return void
	 */
	public function update_active_stream() 
	{
		$stream = isset( $_COOKIE['stream'] ) ? $_COOKIE['stream'] : '';

		if ( !isset( $_GET['stream'] ) /*|| empty( $_GET['stream'] )*/ )
			return $stream;

		$stream = $_GET['stream'];

		setcookie( 'stream', $stream, time() + YEAR_IN_SECONDS );

		return $stream;
	}
}
new ITMWP_Show_Stream;