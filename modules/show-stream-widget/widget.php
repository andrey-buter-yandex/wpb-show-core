<?php
/**
 * Show Stream Select Widget
 *
 * @since 1.0
 */
class ITMWPB_Show_Stream_Widget extends WP_Widget 
{
	function __construct() 
	{
		$widget_ops = array( 'classname' => 'widget_show_stream', 'description' => __( "Add user ability to select what Show Stream to view" ) );
		parent::__construct( 'itmwpb-widget-show-stream', __( 'WPBC Stream', 'itmwpb' ), $widget_ops );
		$this->alt_option_name = 'widget_show_stream';
	}

	function widget($args, $instance) 
	{
		extract($args);

		// This withget is active only when there are streams to select
		if ( !$streams = get_show_streams() )
			return;

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Streams', 'itmwpb' ) : $instance['title'], $instance, $this->id_base );
		$current_stream = get_show_stream();

		echo $before_widget;

		if ( $title)
			echo $before_title . $title . $after_title;

		?>
		<form method="get" action="">
			<select name="stream" onchange="this.form.submit()">
				<?php foreach ( $streams as $stream ): ?>
					<option value="<?php echo $stream['key'] ?>" <?php selected( $stream['key'], $current_stream ); ?>><?php echo $stream['title']; ?></option>
				<?php endforeach ?>
			</select>
		</form>
		<?php

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['title'] = esc_attr( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) 
	{
		$title    = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
<?php
	}
}

