<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

/**
 * Text option type.
 *
 * See @ot_display_by_type to see the full list of available arguments.
 *
 * @param     array     An array of arguments.
 * @return    string
 *
 * @access    public
 * @since     2.0
 */
if ( ! function_exists( 'ot_type_datetimepicker' ) ) {

	function ot_type_datetimepicker( $args = array() ) {
		/* turns arguments array into variables */
		extract( $args );

		/* verify a description */
		$has_desc = $field_desc ? true : false;

		// Build args array for datepicker
		$js_args = array();

		if ( isset( $field_settings ) AND is_array( $field_settings ) ) {
			$js_args = $field_settings;
		}

		?>
		<div class="format-setting type-datetimepicker <?php echo $has_desc ? 'has-desc' : 'no-desc'; ?>">
			<?php if ( $has_desc ): ?>
			<div class="description"><?php echo htmlspecialchars_decode( $field_desc ); ?></div>
			<?php endif; ?>
			<div class="format-setting-inner">
				<div class="option-tree-ui-datetimepicker-input-wrap">
					<input type="text" name="<?php esc_attr_e( $field_name ) ?>" id="<?php esc_attr_e( $field_id ) ?>" value="<?php esc_attr_e( $field_value ) ?>" class="widefat option-tree-ui-input cp_input <?php esc_attr_e( $field_class ) ?>" autocomplete="off" readonly="readonly" />
				</div>
			</div>
		</div><!-- type-datetimepicker -->
		<script>
			jQuery(document).ready(function($) {
				if ( typeof $().timepicker == 'function' ) {
					<?php if ( isset( $field_settings ) AND isset( $field_settings['type'] ) AND $field_settings['type'] == 'time' ) : ?>
						$('#<?php esc_attr_e( $field_id ); ?>').timepicker(<?php echo json_encode( $js_args ); ?>);
					<?php elseif ( isset( $field_settings ) AND isset( $field_settings['type'] ) AND $field_settings['type'] == 'date' ) : ?>
						$('#<?php esc_attr_e( $field_id ); ?>').datepicker(<?php echo json_encode( $js_args ); ?>);
					<?php else : ?>
						$('#<?php esc_attr_e( $field_id ); ?>').datetimepicker(<?php echo json_encode( $js_args ); ?>);
					<?php endif; ?>
				}
			});
		</script>
		<?php
	}

	/* add scripts for metaboxes to post-new.php & post.php */
	add_action( 'admin_enqueue_scripts', 'ot_type_datetimepicker_assests', 11 );

	function ot_type_datetimepicker_assests( $hook ) {
		if ( in_array( $hook, array( 'appearance_page_ot-theme-options', 'post.php', 'post-new.php' ) ) !== false ) {

			wp_enqueue_script( 'jquery-ui-timepicker-addon' );
			wp_enqueue_style( 'jquery-ui-timepicker-addon' );

			wp_enqueue_style( 'jquery-ui', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', false, '1.10.3' );
		}
	}
}