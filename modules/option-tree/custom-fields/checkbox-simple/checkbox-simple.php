<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;
/**
 * Checkbox option type.
 *
 * See @ot_display_by_type to see the full list of available arguments.
 *
 * @param     array     An array of arguments.
 * @return    string
 *
 * @access    public
 * @since     2.0
 */
if ( ! function_exists( 'ot_type_checkbox_simple' ) ) {
  
  function ot_type_checkbox_simple( $args = array() ) {
    
    /* turns arguments array into variables */
    extract( $args );

    /* verify a description */
    $has_desc = $field_desc ? true : false;

    // Default field value
    $field_settings['value'] = isset( $field_settings['value'] ) ? $field_settings['value'] : '1';

    // Onle on create new post set default value
    if ( !isset( $_GET['post'] ) AND isset( $field_settings['std'] ) AND $field_settings['std'] )
      $field_value = $field_settings['std'];

    /* format setting outer wrapper */
    echo '<div class="format-setting type-checkbox ' . ( $has_desc ? 'has-desc' : 'no-desc' ) . '">';
      
      /* description */
      echo $has_desc ? '<div class="description">' . htmlspecialchars_decode( $field_desc ) . '</div>' : '';
      
      /* format setting inner wrapper */
      echo '<div class="format-setting-inner">'; 
      
      echo '<input type="checkbox" name="' . esc_attr( $field_name ) . '" id="' . esc_attr( $field_id ) . '" value="' . esc_attr( $field_settings['value'] ) . '" ' . ( isset( $field_value ) ? checked( $field_value, $field_settings['value'], false ) : '' ) . ' class="option-tree-ui-checkbox ' . esc_attr( $field_class ) . '" />';

      if ( isset( $field_settings['label'] ) )
      	echo '<label for="' . esc_attr( $field_id ) . '">' . esc_attr( $field_settings['label'] ) . '</label>';
      
      echo '</div>';

    echo '</div>';
    
  }
  
}