<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

/**
 * Text option type.
 *
 * See @ot_display_by_type to see the full list of available arguments.
 *
 * @param     array     An array of arguments.
 * @return    string
 *
 * @access    public
 * @since     2.0
 */
if ( ! function_exists( 'ot_type_custom_post_type' ) ) {

	function ot_type_custom_post_type( $args = array() ) {
		global $post;

		/* turns arguments array into variables */
		extract( $args );

		/* verify a description */
		$has_desc = $field_desc ? true : false;

		$query_args = wp_parse_args( $field_settings, array(
			'post_type' => 'post',
			'posts_per_page' => -1, 
			'orderby' => 'title', 
			'order' => 'ASC', 
			'post_status' => 'any',
			'no_posts' => __( 'No posts found' ),
			'select_post' => __( 'Choose One', 'option-tree' )
		));

		// Look for additional args
		if ( isset( $query_args['same_tax'] ) ) {

			if ( $terms = wp_get_object_terms( $post->ID, $query_args['same_tax'], array( 'fields' => 'ids' ) ) AND !is_wp_error( $terms ) ) {
				$query_args['tax_query'] = isset( $query_args['tax_query'] ) ? $query_args['tax_query'] : array();

				$query_args['tax_query'][] = array(
					'taxonomy' => $query_args['same_tax'],
					'fields' => 'ids',
					'terms' => $terms
				);
			} else {
				$query_args['post__in'] = array( -1 );
				if ( empty( $terms ) ) {
					$field_settings['no_posts'] = isset( $field_settings['no_terms'] ) ? $field_settings['no_terms'] : $field_settings['no_posts'];
				}
			}
		}

		$query = new WP_Query( $query_args );
		?>
		<div class="format-setting type-post-type <?php echo $has_desc ? 'has-desc' : 'no-desc'; ?>">
			<?php if ( $has_desc ): ?>
			<div class="description"><?php echo htmlspecialchars_decode( $field_desc ); ?></div>
			<?php endif; ?>
			<div class="format-setting-inner">
				<div class="option-tree-ui-post-type-input-wrap">
					<?php if ( isset( $field_settings['type'] ) AND 'select' == $field_settings['type'] ): ?>
					<select name="<?php esc_attr_e( $field_name ); ?>" id="<?php esc_attr_e( $field_id ); ?>">
						<?php if ( $query->have_posts() ) : ?>
							<option value=""><?php print( $field_settings['select_post'] ); ?></option>
							<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							<option value="<?php esc_attr_e( get_the_ID() ); ?>" <?php selected( $field_value, get_the_ID() ); ?>><?php esc_attr_e( get_the_title() ); ?></option>
							<?php endwhile; ?>
						<?php else : ?>
						<option value=""><?php print( $field_settings['no_posts'] ); ?></option>
						<?php endif; ?>
					</select>
					<?php else : ?>

					<?php endif; ?>
				</div>
			</div>
		</div><!-- type-datetimepicker -->
		<?php
		wp_reset_postdata();
	}

	/* add scripts for metaboxes to post-new.php & post.php */
	add_action( 'admin_enqueue_scripts', 'ot_type_posts_assests', 11 );

	function ot_type_posts_assests( $hook ) {
		if ( in_array( $hook, array( 'appearance_page_ot-theme-options', 'post.php', 'post-new.php' ) ) ) {
			//wp_enqueue_script( 'theme-options-type_custom_post_type', get_template_directory_uri() . '/inc/options/posts/assets/posts.js', false, '1.3' );
		}
	}
}