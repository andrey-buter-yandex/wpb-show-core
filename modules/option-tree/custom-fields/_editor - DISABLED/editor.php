<?php 

/**
 * Editor option type.
 *
 * See @ot_display_by_type to see the full list of available arguments.
 *
 * @param     array     An array of arguments.
 * @return    string
 *
 * @access    public
 * @since     2.0
 */
if ( ! function_exists( 'ot_type_editor' ) ) {
  
	function ot_type_editor( $args = array() ) {

		/* turns arguments array into variables */
		extract( $args );

		/* verify a description */
		$has_desc = $field_desc ? true : false;

		/* format setting outer wrapper */
		echo '<div class="format-setting type-textarea ' . ( $has_desc ? 'has-desc' : 'no-desc' ) . ' fill-area">';

		/* description */
		echo $has_desc ? '<div class="description">' . htmlspecialchars_decode( $field_desc ) . '</div>' : '';

			/* format setting inner wrapper */
			echo '<div class="format-setting-inner">';

			/* build textarea */
			wp_editor( 
				$field_value, 
				esc_attr( $field_id ), 
				array(
					'editor_class'  => esc_attr( $field_class ),
					'wpautop'       => apply_filters( 'ot_wpautop', true, $field_id ),
					'media_buttons' => false,
					'textarea_name' => esc_attr( $field_name ),
					'textarea_rows' => esc_attr( $field_rows ),
					'tinymce'       => apply_filters( 'ot_tinymce', true, $field_id ),              
					'quicktags'     => apply_filters( 'ot_quicktags', array( 'buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,spell,close' ), $field_id )
				) 
			);

			echo '</div>';

		echo '</div>';

	}
  
}