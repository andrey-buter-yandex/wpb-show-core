<?php 

if ( !class_exists( 'OT_Loader' ) )
	return;

class WPBC_Option_Tree_Addon
{
	public static function include_modules() 
	{
		$module = basename( __DIR__ );

		$include_path = WPBC_MODULE_ABS . '/'. $module .'/custom-fields/';

		$partials = array(
			'checkbox-simple',
			'datetimepicker',
			'posts' // use for preroll on episode ('custom_post_type')
		);

		foreach ( $partials as $partial ) {
			require( $include_path . $partial .'/'. $partial .'.php' );
		}
	}
}
WPBC_Option_Tree_Addon::include_modules();