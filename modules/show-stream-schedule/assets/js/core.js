jQuery(document).ready(function($) {

	$('#show-calendar').map(function() {

		/**
		 * Initialize show calendar
		 */
		var calendar = $(this);

		var date = new Date(),
			d = date.getDate() - date.getDay(), // We need week start day
			m = date.getMonth(),
			y = date.getFullYear();

		// Initialize calendar and add event listeners
		calendar.fullCalendar({
			editable: false,
			header: {
				left: '',
				center: '',
				right: ''
			},
			defaultView: 'agendaWeek',
			columnFormat: {
				month: 'ddd',    // Mon
				week: 'dddd', // Monday
				day: 'dddd M/d'  // Monday 9/7
			},
			eventClick: function(calEvent) {
				// eventClickAction(calEvent);
				goToEditEvent(calEvent);
			}
		});

		// Add already scheduled events to calendar if they present
		if ( typeof showEvents !== 'undefined' && $.isArray( showEvents ) ) {
			for (var i = 0; i < showEvents.length; i++) {
				calendar.fullCalendar( 'renderEvent', {
					id: showEvents[i].id,
					title: showEvents[i].title,
					start: new Date(y, m, d + showEvents[i].day, showEvents[i].start.hour, showEvents[i].start.minute),
					end: new Date(y, m, d + showEvents[i].day, showEvents[i].end.hour, showEvents[i].end.minute),
					allDay: false,
					edit: showEvents[i].edit
				});
			};
		}


		/**
		 * initialize Show Form
		 */
		var form = $('#show-schedule-form'),
			showSelect = $('#show-schedule-select'),
			showStartInput = $('#show-start'),
			showEndInput = $('#show-end'),
			dayCheckboxes = $('.show-weekday'),
			messageBox =$('#schedule-messages'),
			clearBtn = $('#clear-schedule'),
			streamSelect = $('#show-stream');

		// On show select change form status to either 'Adding new event' or 'Updating existing event'
		showSelect.on('change', function() {
			var events,
				showId = parseInt(showSelect.val());

			// If there are any events with current id than we are updating
			if ( showId > 0 ) {
				events = calendar.fullCalendar('clientEvents', showId);
				if ( events.length > 0 ) {
					prepareFormUpdate(events);
				}
			} else {
				prepareFormInsert();
			}
		});

		// Change curernt stream value
		streamSelect.on('change', function(e) {
			window.location = streamSelect.val();
		})

		// On form submit either update or insert event
		form.on('submit', function(e) {
			e.preventDefault();

			isFormValid().then(
				function(formData) {
					proceedFormSubmit(formData);
				},
				function (errors) {
					preventFormSubmit(errors);
				}
			)
		});

		// Init dateTimePicker for show tiem range inputs
		showStartInput.add(showEndInput).timepicker({
			timeFormat: 'HH:mm',
			showSecond: false
		});

		// Clear schedule for show
		clearBtn.on('click', function(e) {
			var showId = parseInt(showSelect.val());

			e.preventDefault();

			// Notify server about schedule clear
			notifyShowScheduleUpdated(showId, null, null, true).then(
				function (success) {

					calendar.fullCalendar('removeEvents', showId);

					showFormMessage({
						type: 'updated',
						message: 'Schedule for <b>'+showSelect.find(':selected').text()+'</b> has been cleared.',
						time: 'temporary',
						clear: true
					});

					// Trigger select change to redraw form status
					showSelect.trigger('change');

				}, function(error) {
					console.log(error);
				}
			)
		})

		/**
		 * Provide form validation and retrieve form data on success
		 * @return {object} Deferred promise.
		 */
		function isFormValid() {
			var deferred = new $.Deferred();

			// This can be achieved by getting all form fields data...

			// All fields must be validated
			$.when( validDayCheckbox(), validShowSelect(), validTimeInterval() )
				.then(
					function (days, show, time) {
						deferred.resolve({ days: days, show: show, time: time });
					},
					function (errors) {
						deferred.reject(errors);
					}
				)

			return deferred.promise();
		}

		/**
		 * Validate select field
		 * @return {object} Promise
		 */
		function validShowSelect() {
			var deferred = new $.Deferred(),
				showId = parseInt(showSelect.val());

			// Check if value is checked in select
			if ( typeof showId === 'number' && showId > 0 ) {
				deferred.resolve({
					id: showId,
					title: showSelect.find('option:selected').text()
				});
			} else {
				deferred.reject('Plese select some show.');
			}

			return deferred.promise();
		}

		/**
		 * Validate select field
		 * @return {object} Promise
		 */
		function validTimeInterval() {
			var deferred = new $.Deferred(),
				start = showStartInput.datepicker("getDate"),
				end = showEndInput.datepicker("getDate");

			// Check if value is checked in select
			if ( start !== null && end !== null ) {
				deferred.resolve({
					start: {
						hour: start.getHours(),
						minute: start.getMinutes()
					},
					end: {
						hour: end.getHours(),
						minute: end.getMinutes()
					}
				});
			} else {
				deferred.reject( "Both time intervals must be present and valid" );
			}

			return deferred.promise();
		}

		/**
		 * Validate weekday checkboxes
		 * @return {object} Promise
		 */
		function validDayCheckbox() {
			var deferred = new $.Deferred(),
				checked = dayCheckboxes.filter(':checked'),
				checkedDays = [];

			// At least one day should be checked
			if (checked.length > 0) {
				checked.each(function(i, checkbox) {
					checkedDays.push( parseInt($(checkbox).val()) );
				}).promise().done(function() {
					deferred.resolve(checkedDays);
				})
			} else {
				deferred.reject( "Please select at least one day" );
			}

			return deferred.promise();
		}

		/**
		 * Success form validation callback
		 * @param  {object} formData Data from form fields
		 * @return {void}
		 */
		function proceedFormSubmit(formData) {

			// Frst we need to notify server about schedule update
			notifyShowScheduleUpdated( formData.show.id, formData.days, formData.time ).then(
				function (response) {

					// Update calendar
					updateShowEvent(formData);

					showFormMessage({
						type: 'updated',
						message: 'Schedule for <b>'+formData.show.title+'</b> has been succesfully updated.',
						time: 'temporary',
						clear: true
					});

					// Trigger select change to redraw form status
					showSelect.trigger('change');

				}, function (errors) {

					// Smthing went wrong notify user
					console.log( errors );
					showFormMessage({
						type: 'error',
						message: 'Something went wrong...',
						time: 'temporary',
						clear: true
					});
				}
			)
		}

		/**
		 * Notify server that show schedule was updated
		 * @param  {object} formData Form values object
		 * @return {object}          Ajax promise
		 */
		function notifyShowScheduleUpdated(post_id, days, time, clear) {
			return $.ajax({
				type: 'GET',
				url:  ajaxurl,
				dataType: 'json',
				data: {
					action: 'update_schedule',
					id: post_id,
					days: days,
					time: time,
					clear: clear,
					stream: streamSelect.attr('data-stream')
				}
			});
		}

		/**
		 * Update show schedule,
		 * old events with same id will be removed
		 * @param  {object} formData Form fields values
		 * @return {void}
		 */
		function updateShowEvent(formData) {

			// Remove all show events if necessary
			if ( form.hasClass('update') ) {
				calendar.fullCalendar('removeEvents', formData.show.id);
			}

			// Loop through all days and add events
			for (var i = 0; i < formData.days.length; i++) {
				calendar.fullCalendar( 'renderEvent', {
					id: formData.show.id,
					title: formData.show.title,
					start: new Date(y, m, d + formData.days[i], formData.time.start.hour, formData.time.start.minute),
					end: new Date(y, m, d + formData.days[i], formData.time.end.hour, formData.time.end.minute),
					allDay: false
				});
			};
		}

		/**
		 * Render user message
		 * Right now we are proceeding only singular messages
		 *
		 * Time parametr can be either string temporary (5 seconds)
		 * of integer with milliseconds
		 * 
		 * @param  {object} message Message object
		 * @return {void}
		 */
		function showFormMessage(msg) {
			var message = $('<div class="message '+msg.type+'"><p>'+msg.message+'</p></div>');

			// maybe clear Message box
			if ( msg.hasOwnProperty('clear') && msg.clear ) {
				messageBox.html('');
			}

			// If time parameter for message is set, than remove it after period
			if ( msg.hasOwnProperty('time') ) {
				message.addClass('fade');

				if ( typeof msg.time == 'string' ) {
					msg.time = 5000; // convert to appropriate time
				}
				setTimeout(function() {
					message.remove();
				}, msg.time);
			}

			messageBox.append(message);

		}

		/**
		 * Invalid form data
		 * @param  {string} error String
		 * @return {void}
		 */
		function preventFormSubmit( error ) {

			// Only one error is shown at a time
			showFormMessage({
				type: 'error',
				message: error,
				time: 'temporary',
				clear: true
			});
		}

		function goToEditEvent(calEvent) {
			if (calEvent.hasOwnProperty('edit')) {
				window.location = calEvent.edit;
				// console.log(calEvent.edit);
			}
		}

		/**
		* Callback triggered on single event click
		* Will update current form to 'update' status
		* 
		* @param  {object} calEvent Calendar event object
		* @param  {object} jsEvent  JS event object
		* @param  {object} view     Current view object
		* @return {void}
		*/
		function eventClickAction(calEvent) {
			var weekdays = [],
				events;

			// Set show select value
			showSelect.val( calEvent.id );
			showSelect.trigger('change');

			// Scroll window top so user notice that update can be done throug show form
			$("html, body").animate({ scrollTop: 0 }, "slow");

			showFormMessage({
				type: 'updated',
				message: 'You are editing <b>'+calEvent.title+'</b> .',
				time: 'temporary',
				clear: true
			});
		}

		/**
		 * Change form status for events update action
		 * @param  {array} events Events array
		 * @return {void}
		 */
		function prepareFormUpdate(events) {
			var weekdays = [];

			form.addClass('update');

			// Get weekdays for passed events
			for (var i = 0; i < events.length; i++) {
				weekdays.push( events[i].start.getDay() );
			}

			// Check weekdays for passed events
			$.each( dayCheckboxes ,function(i, checkbox) {
				checkbox = $(checkbox);
				if ( weekdays.indexOf( parseInt(checkbox.val()) ) > -1 ) {
					checkbox.attr('checked', 'checked');
				} else {
					checkbox.removeAttr('checked');
				}
			});

			// Populate date fields
			showStartInput.timepicker("setDate", events[0].start);
			showEndInput.timepicker("setDate", events[0].end);
		}

		/**
		 * Change form status to 'update'
		 * @return {void}
		 */
		function prepareFormInsert() {
			form.removeClass('update');

			// Uncheck all checkboxes
			dayCheckboxes.removeAttr('checked');

			// Clear time range
			showStartInput.add(showEndInput).val('');
		}

	});

});