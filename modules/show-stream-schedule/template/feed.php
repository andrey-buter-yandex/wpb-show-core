<?php
/**
* jQuery Jplayer template
* @uses object global player instance
*/
global $show_schedule;
$i = 0;

header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

/* <?xml version="1.0" encoding="UTF-8"?><!DOCTYPE station SYSTEM "http://a.espncdn.com/xml/espnradio/schedule4.dtd"> */

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?><!DOCTYPE station SYSTEM "http://a.espncdn.com/xml/espnradio/schedule4.dtd">

	<station name="<?php bloginfo( 'name' ) ?>" blurb="*" callsign="*" frequency="*" city="*" state="" zip="*" webcamStreamDefault="*" webcamStreamHiRes="*" webcamOffMessage="*" sid="1912">
		<images>
			<header value="http://imgsrv.z104.com*" width="*" height="*"/>
			<generic value="http://imgsrv.z104.com*" width="*" height="*"/>
			<background value="http://imgsrv.z104.com*" width="*" height="*"/>
		</images>

		<schedule type="*" timezone="0">

			<?php foreach ( $show_schedule->weekdays as $day ): ?>
				<day value="<?php echo $day['label'] ?>">
					<?php foreach ( $show_schedule->shows as $show ):
						if ( !in_array( $day['value'], wpbc_get_show_days( $show->ID, false ) ) )
							continue;

						$start_1 = wpbc_get_the_show_time( $show->ID, 'start', 'g:i:A' );
						$end_1   = wpbc_get_the_show_time( $show->ID, 'end', 'g:i:A' );

						$start_2 = wpbc_get_the_show_time( $show->ID, 'start', 'G' );
						$start_3 = wpbc_get_the_show_time( $show->ID, 'start', 'i' );

						$end_2   = wpbc_get_the_show_time( $show->ID, 'end', 'g:i:A' );
						$end_3   = wpbc_get_the_show_time( $show->ID, 'end', 'i' );

						?>
						<show id="<?php echo $i; ?>" 
							  name="<?php esc_attr_e( $show->post_title ) ?>"
							  graphic="<?php echo esc_url( get_the_post_thumbnail_src( $show->ID, 'thumbnail' ) ) ?>"
							  link="<?php echo esc_url( get_permalink( $show->ID ) ) ?>" 
							  start="<?php esc_attr_e( $start_1 ); ?>" 
							  end="<?php esc_attr_e( $end_1 ); ?>" 
							  startms="<?php echo $show_schedule->get_time_offset( $day['value'], $start_2, $start_3 ) ?>" 
							  endms="<?php echo $show_schedule->get_time_offset( $day['value'], $end_2, $end_3 ) ?>" 
							  multiday="false" webcamGraphic="*" webcam="*" />
					<?php $i++; endforeach; ?>
				</day>
			<?php endforeach ?>

		</schedule>
	</station>