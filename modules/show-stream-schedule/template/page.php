<?php
/**
* jQuery Jplayer template
* @uses object global player instance
*/
global $show_schedule;
?>
<div class="wrap">
	<div id="icon-edit" class="icon32 icon32-posts-post"><br /></div>
	<h2><?php _e( 'Show Schedule', 'show-schedule' ) ?> <a href="<?php $show_schedule->feed_url() ?>" class="add-new-h2">View xml</a></h2>

	<form class="show-schedule-form" id="show-schedule-form" method="get" action="">

		<table class="form-table">
			<tbody>
				<?php if ( $streams = get_show_streams() AND count( $streams ) > 1 ): ?>
				<tr valign="top">
					<th><label><?php _e( 'Select Stream' ) ?></label></th>
					<td>
						<select id="show-stream" data-stream="<?php echo esc_attr( $show_schedule->get_current_stream() ) ?>">
							<?php foreach ( $streams as $key => $stream ): ?>
								<option value="<?php echo add_query_arg( 'stream', $stream['key'] ) ?>" <?php selected( $stream['key'], $show_schedule->get_current_stream() ) ?>><?php echo $stream['title'] ?></option>
							<?php endforeach ?>
						</select>
					</td>
				</tr>
				<?php endif ?>

				<?php if ( false ) : // Disabled now ?>
				<tr valign="top">
					<th><label><?php _e( 'Select show' ) ?></label></th>
					<td><?php wp_dropdown_pages( array( 
						'post_type' => 'show', 
						'id' => 'show-schedule-select',
						'show_option_none' => __( 'Select Show First', 'itmwp2-shchedule' )
					)); ?></td>
				</tr>
				<tr valign="top">
					<th><label><?php _e( 'Time Intervals' ) ?></label></th>
					<td>
						<label>From</label>
						<input type="text" name="" id="show-start" />
						<label>till</label>
						<input type="text" name="" id="show-end" />
					</td>
				</tr>
				<tr valign="top">
					<th><label><?php _e( 'Weekdays' ) ?></label></th>
					<td>
						<?php foreach ( $show_schedule->weekdays as $day ): ?>
							<input class="show-weekday" type="checkbox" name="" id="show-day-<?php echo $day['value']; ?>" value="<?php echo $day['value']; ?>" />
							<label for="show-day-<?php echo $day['value']; ?>"><?php echo $day['label']; ?></label>
							<br />
						<?php endforeach ?>
					</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>

		<?php if ( false ) : // Disabled ?>
		<div class="submitbox">
			<input type="submit" value="Add Show" class="button button-add-show" />
			<input type="submit" value="Update Show" class="button button-primary button-update-show" />
			<a id="clear-schedule" class="submitdelete deletion clear-schedule" href="#clear">Clear Show Schedule</a>
		</div>
		<?php endif; ?>
	</form>

	<div id="schedule-messages"></div>

	<div id="show-calendar"></div>

</div>