<?php

/**
* Listens to episodes playback
* Adds admin interface for viewing stats
*
* NOTICE we assume that all audio on site are episodes (and prerolls)
*/
class Show_Schedule 
{
	/**
	 * Hold schedule paren slug
	 * @var string
	 */
	var $parent_slug = 'edit.php?post_type=show';

	var $menu_title = 'On Air Schedule';

	var $page_title = 'Show Schedule';

	/**
	 * Hold array of weekdays
	 * @var array
	 */
	var $weekdays;

	/**
	 * Hold module name (should be same as dirname)
	 * @var string
	 */
	var $module_name = 'show-schedule';

	/**
	 * Hold module http path
	 * @var string
	 */
	var $http_path;

	/**
	 * Hold absolute module path
	 * @var string
	 */
	var $module_path;

	/**
	 * Hold current page hook
	 * @var string
	 */
	var $hook = '';

	function __construct() 
	{
		// Hold array of weekdays
		$this->set_weekdays();

		$this->module_path = dirname( __FILE__ );

		$this->http_path   = WPBC_MODULE_URI .'/'. basename( $this->module_path );

		if ( is_admin() ) {

			// Add Schedule admin page
			add_action( 'admin_menu', array( $this, 'register_submenu_page') );

			// Enqueue page scripts and styles
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			// Save changes in schedule
			add_action( 'wp_ajax_update_schedule', array( $this, 'update_schedule' ) );
		} else {

			// Maybe redirect 
			add_action( 'do_feed_rss2', array( $this, 'do_feed_schedule' ), 1 );
		}
	}

	/**
	 * Register submenu pages and collect hooks
	 *
	 * @since 1.0
	 * @access public
	 */
	function register_submenu_page() 
	{
		$this->hook = add_submenu_page( $this->parent_slug, $this->page_title, $this->menu_title, 'manage_options', 'show_schedule', array( $this, 'submenu_page_callback' ) );
	}

	function enqueue_scripts( $hook = '' ) 
	{
		if ( !empty( $this->hook ) && $hook == $this->hook ) {

			$datepicker_uri = WPBC_MODULE_URI . '/option-tree/custom-fields/datetimepicker';

			wp_enqueue_script( 'jquery-ui-timepicker-addon', $datepicker_uri .'/js/jquery-ui-timepicker-addon.js', array( 'jquery-ui-datepicker', 'jquery-ui-slider' ), '1.3' );

			wp_enqueue_style( 'jquery-ui', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', false, '1.10.3' );
			wp_enqueue_style( 'jquery-ui-timepicker-addon', $datepicker_uri .'/css/jquery-ui-timepicker-addon.css', false, '1.3' );


			wp_register_script( 'show_schedule', $this->http_path . '/assets/js/core.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'show_schedule' );

			wp_register_style('show_schedule', $this->http_path . '/assets/css/show-schedule.css' , false, '2', false );
			wp_enqueue_style('show_schedule');

			wp_register_script( 'fullcalendar', $this->http_path . '/assets/js/fullcalendar.min.js', array( 'jquery' ), '2.4.0', true );
			wp_enqueue_script( 'fullcalendar' );

			wp_register_style('fullcalendar', $this->http_path . '/assets/css/fullcalendar.css' , false, '2', false );
			wp_enqueue_style('fullcalendar');

			// Set show stream to get proper show attributes
			if ( $stream = $this->get_current_stream() ) {
				set_show_stream( $stream );
			}

			$this->js_settings_object();
		}
	}

	/**
	 * Include page template
	 */
	function submenu_page_callback() 
	{
		load_template( $this->module_path . '/template/page.php' );
	}


	function js_settings_object() 
	{
		$posts = get_posts(  array(
			'numberposts' => -1,
			'post_type'	  => 'show',
		));

		if ( !empty( $posts ) ) {

			// Hold all events
			$events = array();

			foreach ( $posts as $post ) :
				$weekdays = wpbc_get_show_days( $post->ID );

				if ( !empty( $weekdays ) ) {

					// Get start end end dates
					$start = wpbc_get_the_show_time( $post->ID, 'start', 'G:i' );
					$end   = wpbc_get_the_show_time( $post->ID, 'end',   'G:i' );

					if ( !empty( $start ) && !empty( $end ) ) {

						// Parse time
						$start = explode( ':', $start );
						$end   = explode( ':', $end );

						// Loop Through all events and add show for each day as separate event
						foreach ( $weekdays as $day => $checked ) :
							$events[] = array(
								'id'    => $post->ID,
								'title' => $post->post_title,
								'edit'  => get_edit_post_link( $post->ID, '' ),
								'day'   => absint( $day ),
								'start' => array(
									'hour'   => $start[0],
									'minute' => $start[1]
								),
								'end'   => array(
									'hour'   => $end[0],
									'minute' => $end[1]
								),
							);
						endforeach;
					}
				}
			endforeach;

			// Add events as localization data to calendar script
			if ( !empty( $events ) ) {
				wp_localize_script( 'show_schedule', 'showEvents', $events );
			}
		}
	}

	/**
	 * This is callback to ajax request for schedule update
	 * @return void
	 */
	function update_schedule() 
	{	
		// Get show attrributes
		$post_id    = isset( $_GET['id'] ) ? absint( $_GET['id'] ) : 0;

		$start_time = isset( $_GET['time'] ) && isset( $_GET['time']['start'] ) ? $_GET['time']['start'] : array();
		$end_time   = isset( $_GET['time'] ) && isset( $_GET['time']['end'] )   ? $_GET['time']['end'] : array();

		$days       = isset( $_GET['days'] ) && is_array( $_GET['days'] ) ? array_map( 'absint', $_GET['days'] ) : array();
		$clear      = isset( $_GET['clear'] ) ? (bool) $_GET['clear'] : false;
		$stream     = isset( $_GET['stream'] ) ? esc_attr( $_GET['stream'] ) : '';

		if ( $stream ) {
			set_show_stream( $stream );
		}

		// Update show meta
		if ( $post_id && 
			 $start_time && 
			 $end_time && 
			 $days && 
			 wpbc_set_show_time( $post_id, $start_time, $end_time ) && 
			 wpbc_set_show_days( $post_id, $days ) 
		) {
			wp_die( 1 );
		} elseif ( $post_id && $clear ) {

			// Clear show meta
			wpbc_set_show_time( $post_id, null, null, true );
			wpbc_set_show_days( $post_id, null, true );

			wp_die( 1 );
		} else {
			wp_die( 0 );
		}
	}


	function do_feed_schedule() 
	{
		if ( isset( $_GET['schedule'] ) ) {

			// Set Stream
			if ( isset( $_GET['stream'] ) ) {
				set_show_stream( $_GET['stream'] );
			}

			// Get show posts ordered by start time
			$this->grab_feed_schedule();

			load_template( $this->module_path . '/template/feed.php' );
			die;
		}
	}

	function grab_feed_schedule() 
	{
		$posts = get_posts(  array(
			'numberposts' => -1,
			'post_type'	  => 'show',
		));

		if ( !empty( $posts ) ) {

			$sorted = array();
			foreach ( $posts as $post ) {

				$start = wpbc_get_the_show_time( $post->ID, 'start', 'H:i' );
				$end   = wpbc_get_the_show_time( $post->ID, 'end',   'H:i' );

				// Time parametrs are obligatory
				if ( !empty( $start ) && !empty( $end ) ) {
					$sorted[ $start ] = $post;
				}
			}

			ksort( $sorted );
			$posts = $sorted;
		}

		$this->shows = $posts;
	}


	/**
	 * Output feed url
	 */
	function feed_url() 
	{
		echo add_query_arg( array(
			'schedule' => 1,
			'stream'   => isset( $_GET['stream'] ) ? $_GET['stream'] : ''
		), get_bloginfo( 'rss2_url' ) );
	}

	/**
	 * Calculate milliseconds offset for the show, starting with the week start
	 * @param  int $weekday    Week day
	 * @param  string $time    Time string for passed week day must be H:i format
	 * @return int             Millisedonds offset
	 */
	function get_time_offset( $weekday, $hour, $minute ) 
	{
		$offset = 0;

		if ( !empty( $weekday ) )
			$offset += absint( $weekday ) * DAY_IN_SECONDS;

		if ( !empty( $hour ) ) 
			$offset += absint( $hour ) * HOUR_IN_SECONDS;

		if ( !empty( $minute ) ) 
			$offset += absint( $minute ) * MINUTE_IN_SECONDS;

		return $offset * 1000;
	}

	/**
	 * Store weekdays for checkboxes
	 */
	function set_weekdays() 
	{
		$this->weekdays = array(
			array(
				'label' => 'Sunday',
				'value' => '0'
			),
			array(
				'label' => 'Monday',
				'value' => '1'
			),
			array(
				'label' => 'Tuesday',
				'value' => '2'
			),
			array(
				'label' => 'Wednesday',
				'value' => '3'
			),
			array(
				'label' => 'Thursday',
				'value' => '4'
			),
			array(
				'label' => 'Friday',
				'value' => '5'
			),
			array(
				'label' => 'Saturday',
				'value' => '6'
			)
		);
	}

	/**
	 * Get current stream key
	 * @return string Current Stream key
	 */
	function get_current_stream() 
	{
		return isset( $_GET['stream'] ) ? $_GET['stream'] : '';
	}
}

/**
 * This Class is functioning in backend only
 */
$GLOBALS['show_schedule'] = new Show_Schedule;