<?php 

/**
 * WPBC_Episode_Playbacks
 *
 * Бобавляет таб "playback" в widget tabs
 *
 * Add new type to WPB_Widget_Tabbed
 *
 * depends on class Episode_Stats (module "episode-stats")
 */

WPBC_Episode_Playbacks_Tab::load();

class WPBC_Episode_Playbacks_Tab
{
	public static function load()
	{
		if ( !class_exists( 'Episode_Stats' ) )
			return;

		add_filter( 'wpbw_tabs_widget_register_types', array( __CLASS__, 'register_type' ) );
		add_filter( 'wpbw_tabs_widget_get_playbacks_content', array( __CLASS__, 'get_playbacks' ), 10, 2 );
	}

	function register_type( $types )
	{
		$types['playbacks'] = 'Played';

		return $types;
	}

	/**
	 * Get popular posts tab
	 * @param array  widget instance
	 * @return mixed array with tab title and content or false if no posts were found
	 */
	function get_playbacks( $content, $instance ) 
	{
		global $episode_stats, $wpdb;

		// Check if playbacks need to be included
		if ( !isset( $instance['include'] ) )
			return;
	
		if ( is_array( $instance['include'] ) && !in_array( 'playbacks', $instance['include'] ) )
			return;

		// Get all listened episodes
		$sql = "SELECT episode_id, COUNT(*) AS playbacks FROM $episode_stats->table GROUP BY episode_id HAVING COUNT(*)>1 ORDER BY playbacks DESC";
		
		$playbacks = $wpdb->get_results( $sql, OBJECT_K );

		if ( empty( $playbacks ) )
			return;

		$args = array(
			'post_type'      => 'episode',
			'posts_per_page' => $instance['number'],
			'post__in'       => array_map( 'absint', array_keys( $playbacks ) )
		);

		$recent = new WP_Query( $args );

		if ( !$recent->have_posts() ) {
			wp_reset_postdata();
			return false;
		}

		ob_start();
		while ( $recent->have_posts() ) : $recent->the_post();
			get_theme_part( 'widget/recent', 'playback' );
		endwhile;
		$content = ob_get_clean();

		wp_reset_postdata();

		return $content;
	}
}