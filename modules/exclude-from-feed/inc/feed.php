<?php 

/**
* Front page theme handler
*/
class WPBC_Exclude_Feed 
{
	function load() 
	{
		if ( is_admin() )
			add_action( 'save_post', array( __CLASS__, 'remove_empty_post_meta' ), 10 );
		else
			// Exclude posts with exclude meta from feed
			add_action( 'pre_get_posts', array( __CLASS__, 'exclude_posts' ) );
	}

	/**
	 * Exclude specific posts from feed query
	 * @param  objec $query  WP_Query object
	 * @return void
	 */
	function exclude_posts( $query ) 
	{
		remove_action( current_filter(), array( __CLASS__, __FUNCTION__ ) );

		if ( !$query->is_feed() )
			return;

		$meta_query = $query->get( 'meta_query' );

		if ( !$meta_query )
			$meta_query = array();

		$meta_query[] = array(
			'key'     => 'feed_exclude',
			'compare' => 'NOT EXISTS'
		);
		$query->set( 'meta_query', $meta_query );
	}

	/**
	 * Remove empty post meta 'feed_exclude' from table,
	 * because on update and uncheck 'Exclude from feed'
	 * on table stays empty field
	 * and not working 'NOT EXISTS'
	 *
	 * Rus comments:
	 * при обновлении поста, если отчекнут чекбокс, 
	 * в таблице остается пустое поле, 
	 * и проверка mata на 'NOT EXISTS' НЕ работает.
	 */
	function remove_empty_post_meta( $post_id )
	{
		$supperted_types = array(
			'post', 
			'show', 
			'episode'
		);

		if ( !in_array( get_post_type( $post_id ), $supperted_types ) )
			return;

		$meta_key = 'feed_exclude';

		$feed_exclude = get_post_meta( $post_id, $meta_key, true );

		if ( $feed_exclude )
			return;

		delete_post_meta( $post_id, $meta_key );
	}
}
WPBC_Exclude_Feed::load();