<?php 

/**
 * Handle every member aspect
 */

add_filter( 'wpbc_populate_meta_boxes', 'wpbc_populate_feed_meta_boxes' );
function wpbc_populate_feed_meta_boxes( $meta_boxes = array() ) {
	$prefix = 'feed_';
	$meta_boxes[] = array(
		'id'       => 'feed_meta',
		'title'    => __( 'Exclude From Feed', 'itmwp' ),
		'pages'    => array( 'post', 'show', 'episode' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'label'   => '',
				'id'      => $prefix . 'exclude',
				'type'    => 'checkbox',
				'choices' => array(
					array(
						'label' => 'Yes',
						'value' => '1'
					)
				),
			),
		)
	);

	return $meta_boxes;
}