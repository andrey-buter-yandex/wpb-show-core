<?php
/**
 * Exclude from feed choosen posts from 'post', 'show', 'episode'
 */ 

class WPBC_Exclude_From_Feed
{
	public static function include_modules() 
	{
		$module = basename( __DIR__ );

		$include_path = WPBC_MODULE_ABS . '/'. $module .'/inc/';

		$partials = array(
			'feed',
			'meta',
		);

		foreach ( $partials as $partial ) {
			require( $include_path . $partial .'.php' );
		}
	}
}
WPBC_Exclude_From_Feed::include_modules();