<?php

/**
 * Handle NUK Products
 */

class WPBC_Modules 
{
	public static function load() 
	{
		self::define_constanta();
		self::include_partials();
	}

	/**
	 * Load product model partials
	 * @return void
	 */
	private static function include_partials() 
	{
		$partials = array(
			// add custom option-tree fileds
			'option-tree',

			// add meta fileds to taxonomy
			'taxonomy-meta',

			// episode stats
			'episode-stats',

			// add playbacks tab to widget tabs
			'episode-playbacks-tab',

			// use for audio episodes
			'jplayer',

			// show timetable by stream
			'show-stream-schedule',

			// add widget to choose global stream
			'show-stream-widget',

			// create podcats feeds
			'podcast-feed',

			// exclude posts from feed
			'exclude-from-feed'

			// use and enabled on admin episode class WPBC_Admin_Single_Episode() (wpb-core/inc/episode/admin/single-page.class.php)
			// 'getid3'  - DO NOT ENABLE HERE !!!!
		);

		foreach ( $partials as $partial ) {
			require( WPBC_MODULE_ABS .'/'. $partial .'/'. $partial .'.php' );
		}
	}

	/**
	 * Define module constants
	 */
	private static function define_constanta() 
	{	
		$module_path = '/modules';		

		if ( !defined( 'WPBC_MODULE_PATH' ) )
			define( 'WPBC_MODULE_PATH', $module_path );

		$abs_path = WPBC_ABSPATH . WPBC_MODULE_PATH;

		if ( !defined( 'WPBC_MODULE_ABS' ) )
			define( 'WPBC_MODULE_ABS', $abs_path );

		$uri = WPBC_URI . WPBC_MODULE_PATH;

		if ( !defined( 'WPBC_MODULE_URI' ) )
			define( 'WPBC_MODULE_URI', $uri );
	}
}
WPBC_Modules::load();