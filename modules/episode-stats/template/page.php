<?php 
/**
 * Episode statistics page
 */
global $episode_stats, $typenow;
?>
<div class="wrap">
	<h2><?php echo $episode_stats->current_page['page_title'] ?> <a href="<?php echo add_query_arg( 'csv', 1 ) ?>" class="add-new-h2">in csv</a></h2>

	<?php $episode_stats->list_table->views(); ?>

	<form id="playback-filter" method="get" action="">

		<?php $episode_stats->list_table->search_box( $episode_stats->list_table->get_object_label('search_items'), 'post' ); ?>

		<!-- For plugins, we also need to ensure that the form posts back to our current page -->
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
		<input type="hidden" name="post_type" class="post_type_page" value="<?php echo $typenow; ?>" />

		<!-- Now we can render the completed list table -->
		<?php $episode_stats->list_table->display() ?>
	</form>

</div>