jQuery(document).ready(function($) {

	if ( typeof $().timepicker == 'function' ) {

		$('.datetimepicker').each(function(i, e) {
			var options = { 
					dateFormat: 'M d, yy', 
					showSecond: false
				},
				field = $(e);

			if ( field.attr('data-actual-field') ) {
				options.altField = field.attr('data-actual-field');
				options.altFormat = 'yy-mm-dd';
				options.altFieldTimeOnly = false;
				options.altSeparator = 'T';
				options.altTimeFormat = 'HH:mmZ';
			}

			field.datetimepicker(options);

			if ( field.attr('data-actual-field') && $(options.altField).val() != '' ) {
				field.datepicker( 'setDate', new Date( $(options.altField).val() ) );
			}
		});

	}

});