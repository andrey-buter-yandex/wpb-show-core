<?php
/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be movies.
 */
class Podcast_Stats_List_Table extends WP_List_Table {

    /**
     * Hold statistics table name
     * @var string
     */
    var $table;

    /**
     * Hold current taxonomy
     * @var string
     */
    var $taxonomy = 'podcast';

    /**
     * Hold array of query arguments
     * @var array
     */
    var $callback_args;

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct( $args ){
        global $status, $page;

        $screen = isset( $args['screen'] ) ? $args['screen'] : null;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'podcast',     //singular name of the listed records
            'plural'    => 'podcasts',    //plural name of the listed records
            'ajax'      => false,        //does this table support ajax?
            'screen'    => $screen
        ) );

        // Set variables
        $this->table = $args['table'];

        // Init csv exporter 
        add_action( 'stat_list_load', array( $this, 'maybe_output_csv' ) );
    }

    function process_bulk_action() {

        //Detect when a bulk action is being triggered...
        if( 'clean' === $this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }

    function prepare_items() {
        global $wpdb;

        $this->set_column_headers();


        $tags_per_page = $this->get_items_per_page( 'edit_' . $this->screen->taxonomy . '_per_page' );

        $search = !empty( $_REQUEST['s'] ) ? trim( stripslashes( $_REQUEST['s'] ) ) : '';

        $args = array(
            'search' => $search,
            'page'   => $this->get_pagenum(),
            'number' => $tags_per_page,
        );

        if ( !empty( $_REQUEST['orderby'] ) )
            $args['orderby'] = trim( stripslashes( $_REQUEST['orderby'] ) );

        if ( !empty( $_REQUEST['order'] ) )
            $args['order'] = trim( stripslashes( $_REQUEST['order'] ) );

        $this->callback_args = $args;

        $this->set_pagination_args( array(
            'total_items' => wp_count_terms( $this->taxonomy, compact( 'search' ) ),
            'per_page'    => $tags_per_page,
        ) );

        $this->fetch_items();

    }

    /**
     * Define which columns are available in csv file
     * @return array
     */
    function get_csv_columns() {
        $csv_columns = array(
            'title' => 'Title',
            'total' => 'Total',
        );
        return $csv_columns;
    }

    function column_default($item, $column_name){
        return print_r($item,true); //Show the whole array for troubleshooting purposes
    }

    function column_title( $term ){

        //Build row actions
        $actions = array(
            'view' => sprintf( '<a href="%s">View</a>', get_term_link( $term ) ),
        );

        return sprintf('<a href="%1$s">%2$s</a> %3$s',
            get_term_link( $term ),
            $term->name,
            $this->row_actions($actions)
        );
    }

    function csv_column_title( $term ){
        return $term->name;
    }

    function column_cb( $term ){
        return sprintf(
            '<input type="checkbox" name="posts_cb[]" value="%1$s" />',
            $term->term_id
        );
    }
    

    /**
     * Total items column
     * @param  object $post Post object
     * @return void
     */
    function column_total( $term ){
        return sprintf(
            '<span class="total-plays">%s</span>',
            $this->get_total_playbacks( $term )
        );
    }

    /**
     * Total items column
     * @param  object $post Post object
     * @return void
     */
    function csv_column_total( $term ){
        return $this->get_total_playbacks( $term );
    }

    function get_columns(){
        $columns = array(
            'cb'    => '<input type="checkbox" />', //Render a checkbox instead of text
            'title' => 'Title',
            'total' => 'Total',
        );
        return $columns;
    }
    
    function get_sortable_columns() {
        $sortable_columns = array(
            'title' => array( 'title', false ),     //true means it's already sorted
            'total' => array( 'total', false ),
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'clear' => 'Clear Statistics'
        );
        return $actions;
    }

    /**
     * Get an associative array ( id => link ) with the list
     * of views available on this table.
     *
     * Each view links to page with appropriate stats table
     *
     * @since 3.1.0
     * @access protected
     *
     * @return array
     */
    function get_views() {
        global $episode_stats;
        $views = array();

        foreach ( $episode_stats->pages as $page_key => $page ) {

            $permalink = add_query_arg( 'page', $page_key, admin_url( $page['parent_slug'] ) );
            $current_class = $episode_stats->is_current_page( $page_key ) ? 'current' : '';
            $text = $page['short_title'];

            $views[ $page_key ] = sprintf( '<a href="%s" class="%s">%s</a>', $permalink, $current_class, $text );
        }

        return $views;
    }

    /**
     * Query items for table content
     * @return void
     */
    function fetch_items() {
        $taxonomy = $this->taxonomy;

        $args = wp_parse_args( $this->callback_args, array(
            'page' => 1,
            'number' => 20,
            'search' => '',
            'hide_empty' => 0
        ) );

        extract( $args, EXTR_SKIP );

        $args['offset'] = $offset = ( $page - 1 ) * $number;

        // convert it to table rows
        $out = '';
        $count = 0;

        $terms = array();

        $this->items = get_terms( $taxonomy, $args );
    }

    /**
     * Setup Column headers from main columns and sortable
     */
    function set_column_headers() {
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);
    }


    /**
     * Get label for current post type object
     * @param  string $label_key Lable key
     * @return string            Label value
     */
    function get_object_label( $label_key = '' ) {
        $taxonomy = get_taxonomy( 'podcast' );

        return isset( $taxonomy->labels->$label_key ) ? $taxonomy->labels->$label_key : '';
    }

    /**
     * Get total number of times current post has been played
     * @param  object $post Post object
     * @return int          Number of playbacks.
     */
    function get_total_playbacks( $term ) {
        global $wpdb;

        $where = ' 1 <> 1';

        // Get all episodes connected to passed term

        $episodes = wpbc_get_episodes_by( 'podcast', $term->term_id );

        if ( !empty( $episodes ) ) {
            $ids = array();
            foreach ( $episodes as $episode ) {
                $ids[] = $episode->ID;
            }
            $ids = implode( ',', $ids );
            $where = "episode_id IN ($ids)";
        }

        $sql = "SELECT COUNT(*) FROM $this->table WHERE $where";

        return absint( $wpdb->get_var( $sql ) );
    }

    /**
     * Check if csv attribute 
     * @return void
     */
    function maybe_output_csv() {
        if ( isset( $_GET['csv'] ) && $_GET['csv'] ) {
            $this->serve_csv();
        }
    }

    /**
     * Serve statistics as csv
     * @return void
     */
    function serve_csv() {

        $this->csv_send_headers();

        $columns = $this->get_csv_columns();

        // Output headers
        echo '"'.implode( '","', $columns ) . '"';
        echo "\n";

        // Output all items
        foreach ( $this->items as $item ) {
            $col_num = 0;
            foreach ( $columns as $key => $value) {

                if ( method_exists( $this, 'csv_column_'. $key ) ) {
                    echo '"' . esc_attr( call_user_func( array( &$this, 'csv_column_'. $key ), $item ) ) . '"';

                    if ( ++$col_num == count( $columns ) ) {
                        echo "\n";
                    } else {
                        echo ",";
                    }
                }
            }
        }

        die;
    }

    /**
     * Genrate csv filename dependig on current filters 
     * and send headers
     * @return void
     */
    function csv_send_headers() {
        $name = 'podcast-stats';

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$name.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
    }

}
