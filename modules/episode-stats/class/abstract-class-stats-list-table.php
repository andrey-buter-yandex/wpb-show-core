<?php

class Stats_List_Table extends WP_List_Table 
{
    /**
     * Hold statistics table name
     * @var string
     */
    var $table;

    function __construct( $args )
    {
        //Set parent defaults
        parent::__construct( array(
            'singular'  => $post_type->labels->singular_name,
            'plural'    => $post_type->labels->name,
            'ajax'      => false
        ) );

        $this->initialize();
    }

    /**
     * Abstract initilization method
     * @return void
     */
    function initialize() {}

    /**
     * @todo provide content
     * @return void
     */
    function process_bulk_action() 
    {
        //Detect when a bulk action is being triggered...
        if( 'clean'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
    }

    /**
     * Prepare display data
     * @return void
     */
    function prepare_items() 
    {
        // initialize csv actions and filters
        $this->maybe_init_csv();

        $this->init_filters();

        $this->fetch_items();

        $this->set_list_pagination_args();

        do_action( 'items_prepared' );
    }

    /**
     * Abstrac method to init filters
     * @return void
     */
    function init_filters() {}

    /**
     * Get list table items
     * @return void
     */
    function fetch_items() {}

    /**
     * Set pagination args
     */
    function set_list_pagination_args() {}

    /**
     * Define available table bulk actions
     * @return array
     */
    function get_bulk_actions() 
    {
        $actions = array(
            'clean' => __( 'Clear Statistics', 'episode-stat' )
        );
        return $actions;
    }

    /**
     * Get an associative array ( id => link ) with the list
     * of views available on this table.
     *
     * Each view links to page with appropriate stats table
     *
     * @return array
     */
    function get_views() 
    {
        global $episode_stats;

        $views = array();

        foreach ( $episode_stats->pages as $page_key => $page ) {

            $permalink = add_query_arg( 'page', $page_key, admin_url( $page['parent_slug'] ) );
            $current_class = $episode_stats->is_current_page( $page_key ) ? 'current' : '';
            $text = $page['short_title'];

            $views[ $page_key ] = sprintf( '<a href="%s" class="%s">%s</a>', $permalink, $current_class, $text );
        }

        return $views;
    }

    /**
     * Render additional table navigation
     * @param  string $which Nav position string "top" or "bottom"
     * @return void
     */
    function extra_tablenav( $which ) 
    {
?>
        <div class="alignleft actions">
<?php
        do_action( 'extra_stat_tablenav', $which );
        if ( 'top' == $which && !is_singular() ) {
            submit_button( __( 'Filter' ), 'button', false, false, array( 'id' => 'post-query-submit' ) );
        }

?>
        </div>
<?php
    }

    /**
     * Get label for current table object label by key
     * @param  string $label_key Lable key
     * @return string            Label value
     */
    function get_object_label( $label_key = '' ) {}

    /**
     * Maybe initialize csv filters and actions
     * @return void
     */
    function maybe_init_csv() 
    {
        if ( isset( $_GET['csv'] ) && $_GET['csv'] ) {

            add_filter( 'pre_query_items', array( &$this, 'csv_query_items_args' ) );

            add_action( 'items_prepared', array( &$this, 'serve_csv' ) );
        }
    }

    /**
     * This function must be extended by child class
     * @param  array  $query_args List table items query
     * @return array              Filtered query args
     */
    function csv_query_items_args( $query_args = array() ) 
    {
        return $query_args;
    }

    /**
     * Serve statistics as csv
     * @return void
     */
    function serve_csv() 
    {
        $this->csv_send_headers();

        $columns = $this->get_csv_columns();

        // Output headers
        echo '"'.implode( '","', $columns ) . '"';
        echo "\n";

        // Output all items
        foreach ( $this->items as $item ) {
            $col_num = 0;
            foreach ( $columns as $key => $value) {

                if ( method_exists( $this, 'csv_column_'. $key ) ) {
                    echo '"' . esc_attr( call_user_func( array( &$this, 'csv_column_'. $key ), $item ) ) . '"';

                    if ( ++$col_num == count( $columns ) ) {
                        echo "\n";
                    } else {
                        echo ",";
                    }
                }
            }
        }

        die;
    }

    /**
     * Genrate csv filename dependig on current filters 
     * and send headers
     * @return void
     */
    function csv_send_headers() 
    {
        $name = apply_filters( 'csv_filename', $name );

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$name.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
    }
}