<?php

class Player_Stats_List_Table extends WP_List_Table 
{
    /**
     * Hold statistics table name
     * @var string
     */
    var $table;

    /**
     * Hold current post type
     * @var [type]
     */
    var $post_type;

    /**
     * Hold Current query arguments
     * @var array
     */
    var $args;

    /**
     * Hold current wp query instance
     * @var object WP_Query
     */
    var $wp_query;

    /**
     * Hold time filter array
     * @var array of mysql formatted dates 'start', 'end'
     */
    var $time;

    /**
     * Store current podcast term
     * @var object WP_Term object
     */
    var $podcast;

    function __construct( $args )
    {
        // Setup initial variables
        $this->table = $args['table'];
        $this->post_type = $args['post_type'];

        $post_type = get_post_type_object( $this->post_type );

        //Set parent defaults
        parent::__construct( array(
            'singular'  => $post_type->labels->singular_name,
            'plural'    => $post_type->labels->name,
            'ajax'      => false
        ) );

        // Init csv exporter 
        add_action( 'stat_list_load', array( $this, 'maybe_output_csv' ) );
    }

    /**
     * @todo provide content
     * @return void
     */
    function process_bulk_action() 
    {
        //Detect when a bulk action is being triggered...
        if( 'clean' === $this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
    }

    /**
     * Prepare display data
     * @return void
     */
    function prepare_items() 
    {
        // Set time filter options and add query action if necessary
        $this->init_time_filter();

        // Query action is added if necessary
        $this->init_tax_filter();

        $this->query_posts();

        $this->set_internal_pagination_args();
    }

    /**
     * Grab query arguments for current list table
     * And initialize WP_Qeury for current table
     * 
     * @return void
     */
    function query_posts() 
    {
        $q = $_GET;

        // Post type is passed on object creation
        $post_type = $this->post_type;

        $post_status = 'publish';

        if ( isset($q['orderby']) )
            $orderby = $q['orderby'];

        if ( isset($q['order']) )
            $order = $q['order'];

        if ( isset( $q['s'] ) )
            $s = $q['s'];

        $per_page = 'edit_' . $post_type . '_per_page';
        $posts_per_page = (int) get_user_option( $per_page );
        if ( empty( $posts_per_page ) || $posts_per_page < 1 )
            $posts_per_page = 20;


        $this->args = compact( 'post_type', 'post_status', 'order', 'orderby', 'posts_per_page', 's' );

        $this->wp_query = new WP_Query( $this->args );

        $this->items = $this->wp_query->posts;
    }

    function set_internal_pagination_args() 
    {
        $per_page = $this->get_items_per_page( 'edit_' . $this->post_type . '_per_page' );
        $total_items = $this->wp_query->found_posts;
        $total_pages = $this->wp_query->max_num_pages;

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items/$per_page)
        ) );
    }

    /**
     * Define table columns with their headings
     * @return array
     */
    function get_columns()
    {
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'title'     => 'Title',
            'total'     => 'Total',
            'last_time' => 'Last Time'
        );
        return $columns;
    }

    /**
     * Define which columns are sortable
     * @return array
     * @todo  look after sortable parametr
     */
    function get_sortable_columns() 
    {
        $sortable_columns = array(
            'title'     => array( 'title', false ),
            'total'     => array( 'total', false ),
            'last_time' => array( 'last_time', false )
        );
        return $sortable_columns;
    }

    /**
     * Define which columns are available in csv file
     * @return array
     * @todo  look after sortable parametr
     */
    function get_csv_columns() 
    {
        $csv_columns = array(
            'title'     => 'Title',
            'total'     => 'Total',
            'last_time' => 'Last time'
        );
        return $csv_columns;
    }

    /**
     * Define available table bulk actions
     * @return array
     */
    function get_bulk_actions() 
    {
        $actions = array(
            'clear' => 'Clear Statistics'
        );
        return $actions;
    }

    /**
     * Render checkbox to aad ability for selecting multiple posts for bulk action
     * @param  object $post Post object
     * @return void
     */
    function column_cb( $post )
    {
        return sprintf(
            '<input type="checkbox" name="posts_cb[]" value="%1$s" />',
            $post->ID
        );
    }

    /**
     * Render post title with additional post actions
     * @param  object $post Post object
     * @return string
     */
    function column_title( $post )
    {
        //Build row actions
        $actions = array(
            'edit' => sprintf( '<a href="%s">Edit</a>', get_edit_post_link( $post->ID ) ),
            'view' => sprintf( '<a href="%s">View</a>', get_permalink( $post->ID ) ),
        );

        return sprintf('<a href="%1$s">%2$s</a> %3$s',
            get_permalink( $post->ID ),
            $post->post_title,
            $this->row_actions($actions)
        );
    }

    /**
     * Return post title column content
     * @param  object $post Post object
     * @return string
     */
    function csv_column_title( $post )
    {
        return $post->post_title;
    }

    /**
     * Render total playaback column
     * @param  object $post Post object
     * @return void
     */
    function column_total( $post )
    {
        return sprintf(
            '<span class="total-plays">%s</span>',
            $this->get_total_playbacks( $post )
        );
    }

    /**
     * Render total playaback column
     * @param  object $post Post object
     * @return void
     */
    function csv_column_total( $post )
    {
        return $this->get_total_playbacks( $post );
    }

    /**
     * Last time post was played column
     * @param  object $post Post object
     * @return void
     */
    function column_last_time( $post ) 
    {
        $last_time = $this->get_last_playback_time( $post );

        if ( $last_time ) {
            $content = mysql2date( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $last_time );
        } else {
            $content = __( 'Never' );
        }

        return $content;
    }

    /**
     * Last time post was played column
     * @param  object $post Post object
     * @return void
     */
    function csv_column_last_time( $post ) 
    {
        $last_time = $this->get_last_playback_time( $post );

        if ( $last_time ) {
            $content = mysql2date( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $last_time );
        } else {
            $content = __( 'Never' );
        }

        return $content;
    }

    /**
     * Get an associative array ( id => link ) with the list
     * of views available on this table.
     *
     * Each view links to page with appropriate stats table
     *
     * @since 3.1.0
     * @access protected
     *
     * @return array
     */
    function get_views() 
    {
        global $episode_stats;
        $views = array();

        foreach ( $episode_stats->pages as $page_key => $page ) {

            $permalink = add_query_arg( 'page', $page_key, admin_url( $page['parent_slug'] ) );
            $current_class = $episode_stats->is_current_page( $page_key ) ? 'current' : '';
            $text = $page['short_title'];

            $views[ $page_key ] = sprintf( '<a href="%s" class="%s">%s</a>', $permalink, $current_class, $text );
        }

        return $views;
    }

    /**
     * Render additional table navigation
     * @param  string $which Nav position string "top" or "bottom"
     * @return void
     */
    function extra_tablenav( $which ) 
    {
?>
        <div class="alignleft actions">
<?php
        do_action( 'extra_stat_tablenav', $which );
        if ( 'top' == $which && !is_singular() ) {
            submit_button( __( 'Filter' ), 'button', false, false, array( 'id' => 'post-query-submit' ) );
        }

?>
        </div>
<?php
    }

    /**
     * Get total number of times current post has been played
     * @param  object $post Post object
     * @return int          Number of playbacks.
     */
    function get_total_playbacks( $post ) 
    {
        global $wpdb;

        $where = ' 1 <> 1';

        switch ( $post->post_type ) {
            case 'episode':
                $where = "episode_id = $post->ID";
                break;
            case 'preroll':
                $where = "preroll_id = $post->ID";
                break;
            case 'show':

                // First we need to get all episodes ids in that show
                $episodes = wpbc_get_episodes_by( 'show', $post->ID, ARRAY_N );

                if ( $episodes ) {
                    $ids = array();
                    foreach ( $episodes as $episode ) {
                        $ids[] = $episode->ID;
                    }
                    $ids = implode( ',', $ids );
                    $where = "episode_id IN ($ids)";
                }
                break;
            // No deafult case
        }

        // Filter is used by extra time filter and podcast filter
        $where = apply_filters( 'where_total_playbacks', $where );

        $sql = "SELECT COUNT(*) FROM $this->table WHERE $where";

        return absint( $wpdb->get_var( $sql ) );
    }

    /**
     * Get label for current post type object
     * @param  string $label_key Lable key
     * @return string            Label value
     */
    function get_object_label( $label_key = '' ) 
    {
        global $typenow;
        $post_type = get_post_type_object( $typenow );

        return isset( $post_type->labels->$label_key ) ? $post_type->labels->$label_key : '';
    }

    /**
     * Get last time post was played
     * @param  object $post WP post.
     * @return string
     */
    function get_last_playback_time( $post ) 
    {
        global $wpdb;

        switch ( $post->post_type ) {
            case 'episode':
                $sql = "SELECT play_date FROM $this->table WHERE episode_id = $post->ID ORDER BY play_date DESC LIMIT 1";
                break;
            case 'preroll':
                $sql = "SELECT play_date FROM $this->table WHERE preroll_id = $post->ID ORDER BY play_date DESC LIMIT 1";
                break;
            // No deafult case
        }

        return $wpdb->get_var( $sql );
    }

    // Time Filter functions

    /**
     * Setup arguments for time filter
     */
    function init_time_filter() 
    {
        // Render fiilter field
        add_action( 'extra_stat_tablenav', array( &$this, 'extra_time_tablenav' ) );

        $this->time = array();

        if ( isset( $_GET['time_from'] ) && !empty( $_GET['time_from'] ) ) {
            $this->time['start'] = date( 'Y-m-d H:i:s', strtotime( $_GET['time_from'] ) );
            $this->time['start_iso'] = $_GET['time_from'];
        }

        if ( isset( $_GET['time_till'] ) && !empty( $_GET['time_till'] ) ) {
            $this->time['end'] = date( 'Y-m-d H:i:s', strtotime( $_GET['time_till'] ) );
            $this->time['end_iso'] = $_GET['time_till'];
        }

        // Init time filters only if there are some time args
        if ( !empty( $this->time ) ) {

            // Filter main query
            add_action( 'pre_get_posts', array( &$this, 'get_posts_by_time' ) );

            // Filter number of playbacks for time period
            add_filter( 'where_total_playbacks', array( &$this, 'where_total_playbacks_in_period' ) );
        }
            
    }

    /**
     * Get specific posts ids depending on specific query args
     * @param  object ref $query WP_QUery object reference
     */
    function get_posts_by_time( &$query ) 
    {
        global $wpdb;

        remove_action( 'pre_get_posts', array( &$this, 'get_posts_by_time' ) );

        $ids = array();
        $where = '';

        if ( isset( $this->time['start'] ) ) {
            $time = $this->time['start'];
            $where .= " AND play_date > '$time'";
        }

        if ( isset( $this->time['end'] ) ) {
            $time = $this->time['end'];
            $where .= " AND play_date < '$time'";
        }

        // We need either podcasts or posts depending on current post type
        $column = $this->post_type . '_id';

        if ( !empty( $where ) ) {
            $ids = $wpdb->get_col( "SELECT $column FROM $this->table WHERE 1=1 $where" );
            $ids = empty( $ids ) ? array(-1) : array_map( 'absint', $ids );
        }

        $query->set( 'post__in', $ids );
    }

    /**
     * Add time period case to sql totatl playbackst request
     * @param  string $where Deaftul where sql part
     * @return string        Filterd where query
     */
    function where_total_playbacks_in_period( $where ) 
    {
        if ( isset( $this->time['start'] ) ) {
            $time = $this->time['start'];
            $where .= " AND play_date > '$time'";
        }

        if ( isset( $this->time['end'] ) ) {
            $time = $this->time['end'];
            $where .= " AND play_date < '$time'";
        }

        return $where;
    }

    /**
     * Render podcast filter on top of tablenav table
     * @param  string $nav_position Table navigation position
     */
    function extra_time_tablenav( $nav_position = '' ) 
    {
        if ( 'top' == $nav_position ) : ?>
        <input type="text" placeholder="From" size="15" class="datetimepicker" data-actual-field="#js-time-from" value="" /> -
        <input type="text" placeholder="Till" size="15" class="datetimepicker" data-actual-field="#js-time-till" value="" />

        <!-- Actual time fields -->
        <input type="hidden" id="js-time-from" name="time_from" value="<?php echo isset( $this->time['start_iso'] ) ? $this->time['start_iso'] : '' ?>" /> -
        <input type="hidden" id="js-time-till" name="time_till" value="<?php echo isset( $this->time['end_iso'] ) ? $this->time['end_iso'] : '' ?>" />
        <?php endif;
    }

    // Add Specific filters for total playback function

    /**
     * Initialize tax filter if necessary
     * @return [type] [description]
     */
    function init_tax_filter() 
    {
        // Render fiilter field
        add_action( 'extra_stat_tablenav', array( &$this, 'extra_podcast_tablenav' ) );
        
        if ( isset( $_GET['podcast_id'] ) && !empty( $_GET['podcast_id'] ) ) {
            $podcast = get_term( absint( $_GET['podcast_id'] ), 'podcast' );

            if ( !empty( $podcast ) && !is_wp_error( $podcast ) ) {
                $this->podcast = $podcast;

                add_action( 'pre_get_posts', array( &$this, 'get_posts_by_podcast' ) );
            }
        }
    }

    /**
     * Add taxonomy
     * @param  object ref $query WP_QUery object reference
     * @return void
     */
    function get_posts_by_podcast( &$query ) 
    {
        remove_action( 'pre_get_posts', array( &$this, 'get_posts_by_podcast' ) );

        $tax_query = $query->get( 'tax_query' );
        $tax_query[] = array(
            'taxonomy' => 'podcast',
            'field' => 'ids',
            'terms' => $this->podcast->term_id
        );
        $query->set( 'tax_query', $tax_query );
    }

    /**
     * Render podcast filter on top of tablenav table
     * @param  string $nav_position Table navigation position
     */
    function extra_podcast_tablenav( $nav_position = '' ) 
    {
        if ( 'top' == $nav_position )
            wp_dropdown_categories( array(
                'show_option_all' => __( 'View all podcasts' ),
                'hide_empty' => 0,
                'hierarchical' => 1,
                'show_count' => 0,
                'orderby' => 'name',
                'name' => 'podcast_id',
                'selected' => isset( $this->podcast ) ? $this->podcast->term_id : 0,
                'taxonomy' => 'podcast'
            ) );
    }

    /**
     * Check if csv attribute 
     * @return void
     */
    function maybe_output_csv() 
    {
        if ( isset( $_GET['csv'] ) && $_GET['csv'] ) {
            $this->serve_csv();
        }
    }

    /**
     * Serve statistics as csv
     * @return void
     */
    function serve_csv() 
    {
        $this->csv_send_headers();

        $columns = $this->get_csv_columns();

        // Output headers
        echo '"'.implode( '","', $columns ) . '"';
        echo "\n";

        // Output all items
        foreach ( $this->items as $item ) {
            $col_num = 0;
            foreach ( $columns as $key => $value) {

                if ( method_exists( $this, 'csv_column_'. $key ) ) {
                    echo '"' . esc_attr( call_user_func( array( &$this, 'csv_column_'. $key ), $item ) ) . '"';

                    if ( ++$col_num == count( $columns ) ) {
                        echo "\n";
                    } else {
                        echo ",";
                    }
                }
            }
        }

        die;
    }

    /**
     * Genrate csv filename dependig on current filters 
     * and send headers
     * @return void
     */
    function csv_send_headers() 
    {
        $name = $this->post_type . '-stats';

        // Add podcast
        if ( isset( $this->podcast ) ) {
            $name .= '-'. $this->podcast->slug;
        }

        // Add time filters
        if ( isset( $this->time['start'] ) ) {
            $name .= '-' . esc_attr( $this->time['start'] );
        }
        if ( isset( $this->time['end'] ) ) {
            $name .= '-' . esc_attr( $this->time['end'] );
        }

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$name.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
    }
}
