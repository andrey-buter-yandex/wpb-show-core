<?php

/**
* Listens to episodes playback
* Adds admin interface for viewing stats
*
* NOTICE we assume that all audio on site are episodes (and prerolls)
*/
class Episode_Stats {

	/**
	 * Hold module http path
	 * @var string
	 */
	var $http_path;

	/**
	 * Hold stats table name
	 * @var string
	 */
	var $table;

	/**
	 * Hold wp list table instance
	 * @var object
	 */
	var $list_table;

	/**
	 * Hold current post type object
	 * @var object
	 */
	var $episode_stats;

	/**
	 * Hold array of supported post types
	 * @var array
	 */
	var $post_types = array(
		'show',
		'episode',
		'preroll'
	);

	/**
	 * Available stats pages
	 * @var array
	 */
	var $pages = array(
		'episode-stat' => array(
			'post_type'   => 'episode',
			'parent_slug' => 'edit.php?post_type=episode',
			'page_title'  => 'Audio Episodes Stats',
			'menu_title'  => 'Audio Episodes Stats',
			'short_title' => 'Episodes',
			'list_table'  => 'Player_Stats_List_Table'
		),
		'preroll-stat' => array(
			'post_type'   => 'preroll',
			'parent_slug' => 'edit.php?post_type=preroll',
			'page_title'  => 'Audio Pre-roll Stats',
			'menu_title'  => 'Audio Pre-roll Stats',
			'short_title' => 'Pre-rolls',
			'list_table'  => 'Player_Stats_List_Table'
		),
		// No stats page for shows
		/*
		'podcast-stat' => array(
			'post_type'   => '', // This one uses separate class Podcast_Stats_List_Table
			'parent_slug' => 'edit.php?post_type=show',
			'page_title'  => 'Podcasts Stats',
			'menu_title'  => 'Podcasts Stats',
			'short_title' => 'Podcasts',
			'list_table'  => 'Podcast_Stats_List_Table'
		),
		*/
	);

	/**
	 * Hold current page data
	 * @var array
	 */
	var $current_page;

	/**
	 * Hold hooks for created pages
	 * @var array
	 */
	var $hooks = array();

	/**
	 * Add actions and listeners
	 */
	function __construct() {
		global $wpdb;

		// Setup variables
		$this->table = $wpdb->prefix . 'episode_stat';

		$this->http_path = get_template_directory_uri() . '/inc/module/episode-stats';

		if ( is_admin() ) {

			// Add stats admin pages
			add_action( 'admin_menu', array( $this, 'register_submenu_page') );

			// Handle form submit
			add_action( 'current_screen', array( $this, 'init_list_table' ) );

			// Check if stats table exists
			add_action( 'after_switch_theme', array( $this, 'stat_table_exists' ) );

			// Add ajax listeners
			add_action( 'wp_ajax_player_played', array( $this, 'ajax_count_callback' ) );
			add_action( 'wp_ajax_nopriv_player_played', array( $this, 'ajax_count_callback' ) );

			// Enqueue Scripts
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		} else {

			// Maybe count episode playback and serve requested audio
			add_action( 'template_redirect', array( $this, 'maybe_serve_episode' ) );

			// Cover episode enclosure with counting link
			add_filter( 'rss_enclosure', array( $this, 'rss_enclosure' ) );
		}
	}

	/**
	 * Register submenu pages and collect hooks
	 *
	 * @since 1.0
	 * @access public
	 */
	function register_submenu_page() {
		foreach ( $this->pages as $id => $page ) {
			$this->hooks[$id] = add_submenu_page( $page['parent_slug'], $page['page_title'], $page['menu_title'], 'manage_options', $id, array( $this, 'submenu_page_callback' ) );
		}
	}

	/**
	 * Initialize list table instance for current stats page
	 * @return void
	 */
	function init_list_table(  ) {
		$screen = get_current_screen();

		if ( $key = array_search( $screen->id, $this->hooks ) ) {

			if(!class_exists('WP_List_Table')){
			    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
			}

			// Store current page data
			$this->current_page = $this->pages[ $key ];
			$this->current_page['key'] = $key;

			// include all list tables
			include_once( 'class/class-player-stats-list-table.php' );
			include_once( 'class/class-podcast-stats-list-table.php' );

			// Table may be either for custom post type (episode|preroll)
			// or for custom taxonomy (podcast)
			// Currently podcast stats is shown for show page
			$this->list_table = new $this->current_page['list_table'](array(
				'screen'    => get_current_screen(),
				'table'     => $this->table,
				'post_type' => $this->current_page['post_type']
			));

			$this->list_table->prepare_items();

			// Allow stat list object to add bulk process actions
			do_action( 'stat_list_load' );
		}
	}

	/**
	 * Count episode play
	 * @return void
	 */
	function ajax_count_callback() {
		global $wpdb;

		// We are not counting prerolls on separately
		if ( isset( $_GET[ 'type' ] ) && $_GET[ 'type' ] == 'preroll' )
			wp_die( _( 'We are interested in counting audio files only.' ) );

		$episode_id = absint( $_GET['id'] );
		$preroll_id = isset( $_GET['preroll_id'] ) ? absint( $_GET['preroll_id'] ) : 0;

		$this->track_play( $episode_id, $preroll_id );

		wp_die( 1 );
	}


	function track_play( $episode_id, $preroll_id = 0 ) {
		global $wpdb;

		// Get all attributes from POST variable
		$date = current_time( 'mysql' );

		$wpdb->insert(
			$this->table,
			array(
				'episode_id' => $episode_id,
				'preroll_id'  => $preroll_id,
				'play_date'   => $date
			),
			array( '%d', '%d', '%s' )
		);
	}

	/**
	 * Maybe count playback and serve audio
	 * @return void
	 */
	function maybe_serve_episode() {

		if ( isset( $_GET['serve_episode'] ) && $post_id = absint( $_GET['serve_episode'] ) ) {

			$this->track_play( $post_id );
			wp_redirect( get_post_meta( $post_id, 'episode_audio', true ) );
			die();
		}
	}

	/**
	 * Wrap media with counter link
	 * so when you go through this link we will first count episode playback
	 * and then serve requested media
	 * @param  int $episode_id    Episode ID
	 * @param  string $media_url  Http path to media to serve
	 * @return string             Wrapped link
	 */
	function wrap_media( $episode_id, $media_url = '' ) {
		return esc_url( add_query_arg(
			array(
				'serve_episode' => $episode_id
			),
			home_url( basename( $media_url ) )
		) );
	}

	/**
	 * Cover enclosure url with stat counter link
	 * @param  [type] $enclosure [description]
	 * @return [type]            [description]
	 */
	function rss_enclosure( $enclosure ) {

		// Replace enclosure url attirbute with one that will count downloads
		if ( is_feed() && 'episode' == get_post_type() ) {
			$audio = get_post_meta( get_the_ID(), 'episode_audio', true );
			$enclosure = str_replace( $audio, $this->wrap_media( get_the_ID(), $audio ), $enclosure );
		}

		return $enclosure;
	}


	/**
	 * We are on plugins page
	 * @param  string  $key Optional. Page name
	 * @return boolean      
	 */
	function is_current_page( $key = '' ) {

		if ( !empty( $key ) && isset( $this->current_page ) && $this->current_page['key'] == $key )
			return true;

		return false;
	}

	/**
	 * Load episode statistics template
	 * @return void
	 */
	function submenu_page_callback() {
		include_once( 'template/page.php' );
	}

	/**
	 * Check if stats table exists, if no than create it
	 * @return void
	 */
	function stat_table_exists() {
		global $wpdb;

		if ( !$wpdb->query( "SHOW TABLES LIKE '$this->table'" ) ) {

			if ( ! empty($wpdb->charset) )
				$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
			if ( ! empty($wpdb->collate) )
				$charset_collate .= " COLLATE $wpdb->collate";

			$query = "CREATE TABLE $this->table (
  episode_id bigint(20) unsigned NOT NULL default '0',
  preroll_id bigint(20) unsigned NOT NULL default '0',
  play_date datetime NOT NULL default '0000-00-00 00:00:00',
  KEY preroll_id (preroll_id)
) $charset_collate;";

			$wpdb->query( $query );
		}
	}

	function maybe_redirect(){
		// Call for bulk actions here

		if ( ! empty($_REQUEST['_wp_http_referer']) ) {
			 wp_redirect( remove_query_arg( array('_wp_http_referer', '_wpnonce'), stripslashes($_SERVER['REQUEST_URI']) ) );
			 exit;
		}
	}

	function admin_enqueue_scripts( $hook ) {
		if ( in_array( $hook, $this->hooks ) !== false ) {

			wp_enqueue_script( 'jquery-ui-timepicker-addon' );
			wp_enqueue_style( 'jquery-ui-timepicker-addon' );

			wp_enqueue_style( 'jquery-ui', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', false, '1.10.3' );

			wp_enqueue_script( 'episode-stats', $this->http_path .'/js/stats.js', array( 'jquery-ui-timepicker-addon' ) );
		}
	}
}

$GLOBALS['episode_stats'] = new Episode_Stats;