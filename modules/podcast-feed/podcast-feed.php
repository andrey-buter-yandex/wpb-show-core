<?php


add_action( 'pre_get_posts', 'wpbc_podcast_feed' );

/**
 * Initialize itunes podcast actions
 * @param  object $query WP_Query object
 */
function wpbc_podcast_feed( $query ) {
	remove_action( current_filter(), __FUNCTION__ );

	if ( !$query->is_feed() )
		return;

	if ( !$query->get('podcast') )
		return;

	// if ( !( $query->is_feed() AND $query->get('podcast') ) )
	// 	return;

	if ( !isset( $_GET['post_type'] ) )
		$query->set( 'post_type', 'episode' );

	if ( 'episode' == $query->get('post_type') ) {

		// Add itunes xmlns
		add_action( 'rss2_ns','wpbc_podcast_rss2_ns' );

		// Global podcast tags
		add_action( 'rss2_head','wpbc_podcast_rss2_head' );

		// Each podcast item tags
		add_action( 'rss2_item','wpbc_podcast_rss2_item' );
	}
}

/**
 * Itunes Specification
 */
function wpbc_podcast_rss2_ns() {
	echo ' xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"';
	remove_action( current_filter(), __FUNCTION__ );
}

/**
 * Add feed head tags
 */
function wpbc_podcast_rss2_head() { 
	$term = get_queried_object(); ?>

	<?php if ( $copyright = get_theme_term_meta( $term->term_id, $term->taxonomy, 'copyright' ) ): ?>
		<copyright><?php echo $copyright; ?></copyright>
	<?php endif ?>

	<?php if ( $subtitle = get_theme_term_meta( $term->term_id, $term->taxonomy, 'subtitle' ) ): ?>
		<itunes:subtitle><?php echo $subtitle ?></itunes:subtitle>
	<?php endif ?>

	<?php if ( $author = get_theme_term_meta( $term->term_id, $term->taxonomy, 'author' ) ): ?>
		<itunes:author><?php echo $author ?></itunes:author>
	<?php endif; ?>

	<itunes:summary><?php echo $term->description ?></itunes:summary>

	<?php if ( $author ): ?>
	<itunes:owner>
		<itunes:name><?php echo $author ?></itunes:name>
		<?php if ( $author_email = get_theme_term_meta( $term->term_id, $term->taxonomy, 'author_email' ) ): ?>
			<itunes:email><?php echo $author_email ?></itunes:email>
		<?php endif ?>
	</itunes:owner>
	<?php endif ?>

	<?php if ( $src = wp_get_attachment_image_src( absint( get_theme_term_meta( $term->term_id, $term->taxonomy, 'thumbnail' ) ), 'thumbnail' ) ): ?>
		<itunes:image href="<?php echo $src[0] ?>" />
	<?php endif; ?>

	<?php if ( $category = get_theme_term_meta( $term->term_id, $term->taxonomy, 'category' ) ): ?>
		<itunes:category text="<?php esc_attr_e( $category ) ?>"/>
	<?php endif; ?>

	<?php 
}

/**
 * Each episode specific itunes tags
 */
function wpbc_podcast_rss2_item() {
	?>

	<itunes:author><?php the_author() ?></itunes:author>

	<?php if ( $subtitle = get_the_post_meta('episode_itunes_subtitle')): ?>
	<itunes:subtitle><?php echo $subtitle; ?></itunes:subtitle>
	<?php endif ?>

	<itunes:summary><?php the_excerpt_rss(); ?></itunes:summary>

	<?php if ( $image = get_the_post_thumbnail( get_the_ID(), 'thumbnail' ) ):

		// We'll need only image src attribute
		preg_match("/(<img[^>]*src *= *[\"']?)([^\"']*)/", $image, $image );
	?>
		<itunes:image href="<?php echo $image[2] ?>" />
	<?php endif ?>

	<?php if ( $duration = get_the_post_meta( 'episode_audio_duration' ) ): ?>
	<itunes:duration><?php echo $duration; ?></itunes:duration>
	<?php endif ?>

	<?php 
}