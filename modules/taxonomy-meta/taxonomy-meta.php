<?php
/**
 * Theme Feed logic
 */ 


class WPBC_Taxonomy_Meta
{
	public static function include_modules() 
	{
		$module = basename( __DIR__ );

		$include_path = WPBC_MODULE_ABS . '/'. $module .'/inc/';

		$partials = array(
			'config',
			'taxonomy-meta.class',
		);

		foreach ( $partials as $partial ) {
			require( $include_path . $partial .'.php' );
		}
	}
}
WPBC_Taxonomy_Meta::include_modules();