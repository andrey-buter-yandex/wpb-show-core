<?php
/**
 * Registering meta sections for taxonomies
 *
 * All the definitions of meta sections are listed below with comments, please read them carefully.
 * Note that each validation method of the Validation Class MUST return value.
 *
 * You also should read the changelog to know what has been changed
 *
 */

// Public function to retrieve term meta 

/**
 * Get term meta
 * @param  int  $term_id  Term Id
 * @param  string  $taxonomy Taxonomy
 * @param  string  $key      Meta Key
 * @param  mixed $default    Deafult return value
 * @return mixed
 */
if ( !function_exists( 'get_theme_term_meta' ) ) {
	function get_theme_term_meta( $term_id, $taxonomy, $key, $default = false ) {

		// Get all taxonomy meta
		$meta = get_option( $taxonomy.'_meta' );

		if ( is_array( $meta ) && isset( $meta[ $term_id ] ) && isset( $meta[ $term_id ][ $key ] ) ) {
			return $meta[ $term_id ][ $key ];
		}

		return $default;
	}
}
	

// Plugin is included as separate module
// require_once( 'taxonomy-meta.class.php' );

// Hook to 'admin_init' to make sure the class is loaded before
// (in case using the class in another plugin)
add_action( 'admin_init', 'itmwp_register_taxonomy_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function itmwp_register_taxonomy_meta_boxes() {
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Taxonomy_Meta' ) )
		return;

	$meta_sections = apply_filters( 'wpbc_taxonomy_meta_fields', array() );

	/*
	// Second meta section
	$meta_sections[] = array(
		'title' => 'Advanced Fields',
		'id'    => 'option_name',

		'fields' => array(
			// CHECKBOX LIST
			array(
				'name'    => 'Checkbox list',
				'id'      => 'checkbox_list',
				'type'    => 'checkbox_list',
				'options' => array(                     // Array of value => label pairs for radio options
					'value1' => 'Label 1',
					'value2' => 'Label 2'
				),
				'desc'    => 'What do you do in free time?'
			),
			// WYSIWYG
			array(
				'name' => 'WYSIWYG Editor',
				'id'   => 'wysiwyg',
				'type' => 'wysiwyg',
			),
			// DATE PICKER
			array(
				'name'   => 'Date Picker',
				'id'     => 'date',
				'type'   => 'date',
				'format' => 'd MM, yy',                // Date format, default yy-mm-dd. Optional. See: http://goo.gl/po8vf
			),
			// TIME PICKER
			array(
				'name'   => 'Time Picker',
				'id'     => 'time',
				'type'   => 'time',
				'format' => 'hh:mm:ss',                // Time format, default hh:mm. Optional. See: http://goo.gl/hXHWz
			),
			// FILE
			array(
				'name' => 'File',
				'id'   => 'file',
				'type' => 'file',
			),
			// IMAGE
			array(
				'name' => 'Image',
				'id'   => 'image',
				'type' => 'image',
			),
			// COLOR PICKER
			array(
				'name' => 'Color Picker',
				'id'   => 'color',
				'type' => 'color',
			),
		),
	);
	*/

	foreach ( $meta_sections as $meta_section ) {
		new RW_Taxonomy_Meta( $meta_section );
	}
}
