<?php 

/**
 * 
 */
class WPBC_Load_Section
{
	/**
	 * Module name
	 */
	public static $section;

	/**
	 * Include path
	 */
	protected static $include_path;

	/**
	 * Folder of section
	 */
	protected static $folder = 'inc';

	/**
	 * Uri path
	 */
	protected static $uri_path;

	/**
	 * List section partials
	 */
	public static $partials = array();

	/**
	 * Pre load function
	 */
	public static function load()
	{
		if ( ! static::$section )
			return;

		static::$include_path = WPBC_ABSPATH . '/'. static::$folder .'/'. static::$section .'/';

		static::pre_load();
		static::include_partials();
		static::on_load();
	}

	/**
	 * Preload function
	 */
	public static function pre_load() {}

	/**
	 * Onload function
	 */
	public static function on_load() {}

	/**
	 * Load product model partials
	 * @return void
	 */
	protected static function include_partials( $partials = array() ) 
	{
		$partials = !empty( $partials ) ? $partials : static::$partials;

		foreach ( $partials as $partial ) {
			require_once( static::$include_path . $partial .'.php' );
		}
	}

	/**
	 * Load deprecated functions
	 */
	public function load_deprecated()
	{
		$partials = array(
			'deprecated'
		);

		static::include_partials( $partials );
	}
}