<?php
/*
Plugin Name: WPB Core 
Description: Shows-Episodes-Preroll
Author: WPB
Version: 1.0
Author URI: http://wordpressforbroadcasters.com/
License: GPLv2 or later
Text Domain: rssi
Domain Path: /i18n
*/

/**
 * Current plugin version
 * Each time on plugin initialization 
 * we are checking this version with one stored in plugin options
 * and if they don't match plugin update hook will be fired
 *
 * @since 2.1
 */
if ( !defined( 'WPBC_VERSION' ) )
	define( 'WPBC_VERSION', '1.0' );

/**
 * The name of the Plugin Core file
 *
 * @since 2.0
 */
if ( !defined( 'WPBC_FILE' ) )
	define( 'WPBC_FILE', __FILE__ );

/**
 * Absolute location of Plugin
 *
 * @since 2.0
 */
if ( !defined( 'WPBC_ABSPATH' ) )
	define( 'WPBC_ABSPATH', dirname( WPBC_FILE ) );

/**
 * The name of the directory
 *
 * @since 2.0
 */
if ( !defined( 'WPBC_DIRNAME' ) )
	define( 'WPBC_DIRNAME', basename( WPBC_ABSPATH ) );

/**
 * The name of the directory
 *
 * @since 2.0
 */
if ( !defined( 'WPBC_URI' ) )
	define( 'WPBC_URI', plugins_url( '', WPBC_FILE ) );

/**
 * Enable debug mode
 *
 * @since 2.0
 */
if ( !defined( 'WPBC_DEBUG' ) )
	define( 'WPBC_DEBUG', false );


add_action( 'plugins_loaded', 'wpbc_pre_setup_plugin' );
function wpbc_pre_setup_plugin() {

	// TEMP: Hide the settings & documentation pages.
	add_filter( 'ot_show_pages', '__return_false' );

	require_once( WPBC_ABSPATH . '/lib/load-section.abstract.php' );
	require_once( WPBC_ABSPATH . '/inc/shared.php' );
}

add_action( 'after_setup_theme', 'wpbc_setup_plugin', 1 );
function wpbc_setup_plugin() {
	// Require plugin essential files
	// All of them are top level inside includes
	// inner dependencies are loaded by this main classes

	// require_once( WPBC_ABSPATH . '/inc/general.php' );
	require_once( WPBC_ABSPATH . '/inc/stream.php' );
	require_once( WPBC_ABSPATH . '/inc/taxonomies.php' );
	require_once( WPBC_ABSPATH . '/inc/show.php' );
	require_once( WPBC_ABSPATH . '/inc/episode.php' );
	require_once( WPBC_ABSPATH . '/inc/preroll.php' );
	require_once( WPBC_ABSPATH . '/modules/modules.php' );
}