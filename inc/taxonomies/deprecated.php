<?php 

/**
 * Get thumbnail id for term
 *
 * get_theme_term_thumbnail_id
 * @param  string $taxonomy Term taxonomy
 * @param  int  $term_id    Term id
 * @return int              Term thumbnail id
 */
if ( !function_exists( 'get_theme_term_thumbnail_id' ) ) {
	function get_theme_term_thumbnail_id( $term_id, $taxonomy ) {
		return wpbc_get_term_thumbnail_id( $term_id, $taxonomy );
	}
}
