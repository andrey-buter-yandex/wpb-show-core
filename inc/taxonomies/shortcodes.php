<?php 


add_shortcode( 'collection', 'wpbc_collection_shortcode' );
function wpbc_collection_shortcode( $atts = array() ) {
	$atts = wp_parse_args( $atts, array(
		'title'     => '',
		'post_type' => 'post',
	) );

	$post_type = explode( ',', $post_type );

	if ( isset( $atts['collection_id'] ) ) {
		$atts['tax_query'] = array(
			array(
				'taxonomy' => 'collection',
				'field'    => 'id',
				'terms'    => array_map( 'absint', explode( ',', $atts['collection_id'] ) )
			)
		);
	}

	$GLOBALS['section_query'] = new WP_Query( $atts );

	// Add Section title to WP_Query args
	$GLOBALS['section_query']->set( 'section-title', $title );

	ob_start();
	get_theme_part( 'section', 'posts' );
	unset( $GLOBALS['section_query'] );

	return ob_get_clean();
}