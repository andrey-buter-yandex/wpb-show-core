<?php

/*
 Each podcast is a category, which may contain several audio posts 
 Prerolls can be associated with this category too,
 When editing single audio, there is an ability to select preroll, from the list of prerolls,
 associaled with the same podcast category
 */
register_taxonomy(
	'podcast',
	array( 'show', 'post' ),
	array(
    	'hierarchical' => true,
	    'labels'       => array(
			'name'              => __( 'Podcasts', 'itmwp2' ),
			'singular_name'     => __( 'Podcast', 'itmwp2' ),
			'search_items'      => __( 'Search Podcasts', 'itmwp2' ),
			'all_items'         => __( 'All Podcasts', 'itmwp2' ),
			'parent_item'       => __( 'Parent Podcast', 'itmwp2' ),
			'parent_item_colon' => __( 'Parent Podcast:', 'itmwp2' ),
			'edit_item'         => __( 'Edit Podcast', 'itmwp2' ), 
			'update_item'       => __( 'Update Podcast', 'itmwp2' ),
			'add_new_item'      => __( 'Add New Podcast', 'itmwp2' ),
			'new_item_name'     => __( 'New Podcast Name', 'itmwp2' ),
			'menu_name'         => __( 'Podcasts', 'itmwp2' ),
		),
	    'show_ui'     => true,
	    'query_var'   => true,
	    'rewrite'     => true,
));


register_taxonomy(
	'collection',
	array( 'show', 'post' ),
	array(
    	'hierarchical' => true,
	    'labels'       => array(
			'name'              => __( 'Push Bins', 'itmwp2' ),
			'singular_name'     => __( 'Push Bin', 'itmwp2' ),
			'search_items'      => __( 'Search push bins', 'itmwp2' ),
			'all_items'         => __( 'All Collections', 'itmwp2' ),
			'parent_item'       => __( 'Parent Push Bin', 'itmwp2' ),
			'parent_item_colon' => __( 'Parent Push Bin:', 'itmwp2' ),
			'edit_item'         => __( 'Edit Push Bin', 'itmwp2' ), 
			'update_item'       => __( 'Update Push Bin', 'itmwp2' ),
			'add_new_item'      => __( 'Add New', 'itmwp2' ),
			'new_item_name'     => __( 'New Name', 'itmwp2' ),
			'menu_name'         => __( 'Push Bins', 'itmwp2' ),
		),
	    'show_ui'     => true,
	    'query_var'   => true,
	    'rewrite'     => true,
));