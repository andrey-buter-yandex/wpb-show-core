<?php 

/**
 * Create HTML dropdown list of Categories.
 *
 * admin panel
 *
 * @package WordPress
 * @since 2.1.0
 * @uses Walker
 */
class WPBC_Walker_PodcastCategoryDropdown extends Walker_CategoryDropdown {
	/**
	 * @see Walker::start_el()
	 * @since 2.1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $category Category data object.
	 * @param int $depth Depth of category. Used for padding.
	 * @param array $args Uses 'selected' and 'show_count' keys, if they exist.
	 */
	function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
		$pad = str_repeat('&nbsp;', $depth * 3);

		$cat_name = apply_filters( 'list_cats', $category->name, $category );
		$output .= "\t<option class=\"level-$depth\" value=\"".$category->slug."\"";

		if ( $category->slug == $args['selected'] )
			$output .= ' selected="selected"';

		$output .= '>';
		$output .= $pad.$cat_name;

		if ( $args['show_count'] )
			$output .= '&nbsp;&nbsp;('. $category->count .')';
		
		$output .= "</option>\n";
	}
}