<?php 

/**
 * Do actions on posts list table page (edit.php)
 */
class WPBC_Admin_Episodes_List_Table
{
	public static function load()
	{
		global $pagenow;

		if ( !isset( $pagenow ) )
			return;

		// Specific admin features
		if ( 'edit.php' != $pagenow )
			return;

		// Add podcast taxonomy as episode list column
		add_filter( 'manage_taxonomies_for_episode_columns', array( __CLASS__, 'add_podcast_category_column' ) );

		// Add filter select for available locations
		add_action( 'restrict_manage_posts', array( __CLASS__, 'podcast_select' ) );
	}

	/**
	 * Add attachment taxonomy as post-list taxonomy column
	 * @param [type] $taxonomies [description]
	 */
	public function add_podcast_category_column( $taxonomies = array() ) 
	{
		$taxonomies[] = 'podcast';
		return $taxonomies;
	}

	/**
	 * Render Location select filter on wp-posts-list-table template
	 * @return void
	 */
	public function podcast_select() 
	{
		$selected = isset( $_GET['podcast'] ) ? $_GET['podcast'] : '';

		$dropdown_options = array(
			'show_option_all' => __( 'View all Podcasts' ),
			'hide_empty'      => 0,
			'hierarchical'    => 1,
			'show_count'      => 0,
			'orderby'         => 'name',
			'selected'        => $selected,
			'name'            => 'podcast',
			'taxonomy'        => 'podcast',
			'walker'          => new WPBC_Walker_PodcastCategoryDropdown
		);
		
		wp_dropdown_categories( $dropdown_options );
	}
}
if ( is_admin() )
	WPBC_Admin_Episodes_List_Table::load();