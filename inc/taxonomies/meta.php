<?php 

add_filter( 'wpbc_taxonomy_meta_fields', 'wpbc_podcast_meta_fileds' );
function wpbc_podcast_meta_fileds( $meta_sections = array() ) {
	// First meta section
	$meta_sections[] = array(
		'title'      => 'Attributes',          // section title
		'taxonomies' => array( 'podcast' ),    // list of taxonomies. Default is array( 'category', 'post_tag' ). Optional
		'id'         => 'podcast_meta',        // ID of each section, will be the option name

		'fields' => array(                     // List of meta fields
			array(
				'name' => 'Thumbnail',
				'id'   => 'thumbnail',
				'type' => 'image',
			),

			array(
				'name' => 'Subtitle',
				'id'   => 'subtitle',
				'type' => 'text',
			),
			array(
				'name' => 'Copyright',
				'id'   => 'copyright',
				'type' => 'text',
			),
			array(
				'name' => 'Author',
				'id'   => 'author',
				'type' => 'text',
			),
			array(
				'name' => 'Author Email',
				'id'   => 'author_email',
				'type' => 'text',
			),
			array(
				'name' => 'Category',
				'id'   => 'category',
				'type' => 'text',
			),
		),
	);

	return $meta_sections;
}