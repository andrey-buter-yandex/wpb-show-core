<?php 


if ( is_admin() ) {
	add_action( 'podcast_edit_form_fields', 'add_taxonomy_rss_link_fields', 10, 2 );
	add_action( 'collection_edit_form_fields', 'add_taxonomy_rss_link_fields', 10, 2 );
}
/**
 * Add rss links to taxonomy edit screen
 * @param object $tag      term object
 * @param string $taxonomy [description]
 */
function add_taxonomy_rss_link_fields( $tag, $tax ) {
	$link = get_term_feed_link( $tag->term_id, $tax );
	$taxonomy = get_taxonomy( $tax )
	?>
		<tr class="form-field">
			<th scope="row" valign="top"><label><?php _e( 'Feed links', 'itmwp2' ); ?></label></th>
			<td>
				<table>
					<tr>
						<td><?php _e( 'Main feed', 'itmwp2' ); ?></td>
						<td><a href="<?php echo $link ?>"><?php echo $link ?></a></td>
					</tr>
					<?php if ( count( $taxonomy->object_type ) > 1 ) :
						foreach ( $taxonomy->object_type as $post_type ) :
							$post_type_url = add_query_arg( 'post_type', $post_type, $link );
							printf( __( '<tr><td>%s feed</td><td><a href="%s">%s</a></td></tr>', 'itmwp2' ), ucfirst( $post_type ), $post_type_url, $post_type_url );
							echo "\n";
						endforeach;
					endif; ?>
				</table>
				
			</td>
		</tr>
	<?php
}


/**
 * Get thumbnail id for term
 *
 * get_theme_term_thumbnail_id
 * @param  string $taxonomy Term taxonomy
 * @param  int  $term_id    Term id
 * @return int              Term thumbnail id
 */
function wpbc_get_term_thumbnail_id( $term_id, $taxonomy ) {
	if ( empty( $term_id ) )
		return 0;

	if ( 'podcast' != $taxonomy )
		return 0;

	$thumb_id = get_theme_term_meta( absint( $term_id ), $taxonomy, 'thumbnail' );

	// Term thumbnail is provided by taxonomy-meta module
	// which saves podcast meta to podcast_meta option
	if ( !empty( $thumb_id ) && is_array( $thumb_id ) )
		return absint( array_shift( $thumb_id ) );

	return 0;
}
