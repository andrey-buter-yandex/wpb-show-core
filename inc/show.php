<?php
/**
 * Handle NUK Products
 */

class WPBC_Model_Show extends WPBC_Load_Section
{
	protected static $include_path;
	
	public static $section = 'show';

	public static $partials = array(
		'model',
		'meta',
		'utils',
		'lib/show-query.class',
		'lib/filters-front-end.class',
		'lib/time-query.class',
		'public-lib',
		'deprecated'
	);

	// public static function on_load() 
	// {
	// 	add_action( 'init', array( __CLASS__, 'load_deprecated' ), 20 );
	// }
}
WPBC_Model_Show::load();