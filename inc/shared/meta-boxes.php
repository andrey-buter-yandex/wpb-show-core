<?php
/**
 * Register meta boxes
 * Including the meta boxes class
 * Meta boxes are added using options tree plugin
 */

/**
 * This module is backend only and for special pages
 */
global $pagenow;

if ( !is_admin() )
	return;

// Hide the settings & documentation pages.
add_filter( 'ot_show_pages', '__return_false' );


// Include OptionTree after providing all necessary filters
$path = get_template_directory() . '/option-tree/ot-loader.php';

if ( file_exists( $path ) ) {
	// Enable Theme mode
	add_filter( 'ot_theme_mode', '__return_true' );

	include_once( $path );
}
	

/**
 * Register meta boxes
 *
 * @return void
 */
add_action( 'admin_init', 'wpbc_register_meta_boxes' );
function wpbc_register_meta_boxes() {

	$meta_boxes = apply_filters( 'wpbc_populate_meta_boxes', array() );

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !function_exists( 'ot_register_meta_box' ) )
		return;

	foreach ( $meta_boxes as $meta_box )
	{
		if ( isset( $meta_box['only_on'] ) && !wpbc_meta_box_maybe_include( $meta_box['only_on'] ) )
			continue;

		//new RW_Meta_Box( $meta_box );
		ot_register_meta_box( $meta_box );
	}
	// unset global variable
	unset( $meta_boxes );
}

/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function wpbc_meta_box_maybe_include( $conditions ) {
	// Include in back-end only
	if ( !defined( 'WP_ADMIN' ) || !WP_ADMIN )
		return false;

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
		return true;


	if ( isset( $_GET['post'] ) )
		$post_id = $_GET['post'];
	elseif ( isset( $_POST['post_ID'] ) )
		$post_id = $_POST['post_ID'];
	else
		$post_id = false;

	$post_id = (int) $post_id;

	foreach ( $conditions as $cond => $v )
	{
		switch ( $cond )
		{
			case 'id':
				if ( !is_array( $v ) )
					$v = array( $v );
				return in_array( $post_id, $v );
			break;
			case 'slug':
				if ( !is_array( $v ) )
					$v = array( $v );
				$post = get_post( $post_id );
				return in_array( $post->post_name, $v );
			break;
			case 'template':
				if ( !is_array( $v ) )
					$v = array( $v );
				return in_array( get_post_meta( $post_id, '_wp_page_template', true ), $v );
			case 'function':
				if (!empty($v) AND function_exists('rw_'.$v))
					return call_user_func('rw_'.$v);
			case 'meta':
				if (is_array($v) AND (1==count($v)))
				{// We can have only one pair of meta-key and meta value to check
					$result = false;
					foreach ($v as $meta_key => $meta_value) {
						$result = ($meta_value==get_post_meta($post_id, $meta_key, true));
						if (!$result)
							break;
					}
					return $result;
				}
				elseif (false)
				{// We can have several pair of meta to check with conditional operator
					foreach ($v as $meta_key => $meta_value) {
						
					}
				}
				break;
			default:
				
				break;
		}
	}

	// If no condition matched
	return false;
}



/**
 * Add upload imade id filed
 *
 * @return bool
 */
add_action( 'save_post','wpbc_add_id_upload_image', 10,2 );
function wpbc_add_id_upload_image( $post_id, $post_object ) {
	global $pagenow, $meta_boxes;

	/* don't save during quick edit */
	if ( $pagenow == 'admin-ajax.php' )
		return $post_id;

	/* don't save during autosave */
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_id;

	/* don't save if viewing a revision */
	if ( $post_object->post_type == 'revision' )
		return $post_id;

	/* check permissions */
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) )
			return $post_id;
	} else {
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return $post_id;
	}

	$meta_box_ids_list = array(
		
	);

	//add to meta-boxes id upload image
	foreach ( $meta_box_ids_list as $meta_box_id ) {
		$metabox = get_post_meta( $post_id, $meta_box_id );
		$metabox = $metabox[0];

		//add to list-item uploads
		if ( is_array( $metabox ) ) {
			$meta_key = $meta_box_id;

			foreach ( $metabox as $key => $item ) {
				foreach ( $item as $field => $field_value ) {
					if ( $field == 'image' ) {
						$metabox[$key]['image_id'] = get_image_id_by_url( $field_value );
					}
				}
			}

			$meta_value = $metabox;
			update_post_meta( $post_id, $meta_key, $meta_value );
		} 		
	}

	$meta_box_ids_single = array(
		
	);

	//add to meta-boxes id upload image to single uploads
	foreach ( $meta_box_ids_single as $meta_box_id ) {
		$meta_key = $meta_box_id.'_id';
		$metabox = get_post_meta( $post_id, $meta_box_id, true );
		$meta_value = get_image_id_by_url( $metabox );

		if ( get_post_meta( $post_id, $meta_key, true ) !== false ) {
			update_post_meta( $post_id, $meta_key, $meta_value );
		} else {
			add_post_meta( $post_id, $meta_key, $meta_value );
		}
	}
}