<?php 

/**
 * 
 */
class WPBC_
{
	function __construct(argument)
	{
		# code...
	}


	/**
	 * This is rewrite of default function get_template_part
	 * Add ability to searches for theme files in subdirectories
	 */
	function get_theme_part( $slug, $name = null, $return = false ) {
		$templates = array();
		if ( isset($name) )
		{
			$templates[] = "{$slug}/{$name}.php";
			$templates[] = "{$slug}-{$name}.php";
		}

		$templates[] = "{$slug}.php";

		if ( $return )
			ob_start();

		locate_template( $templates, true, false );

		if ( $return )
			return ob_get_clean();
	}


	/**
	 * Retrieve the name of the highest priority template file that exists.
	 *
	 * Searches in the GS_URL before GS_ABSPATH so that themes which
	 * inherit from a parent theme can just overload one file.
	 *
	 * See WP locate_template()
	 *
	 * @since 2.7.0
	 *
	 * @param string|array $template_names Template file(s) to search for, in order.
	 * @param bool $load If true the template file will be loaded if it is found.
	 * @param bool $require_once Whether to require_once or require. Default true. Has no effect if $load is false.
	 * @return string The template filename if one is located.
	 */
	function locate_template( $template_names, $load = false, $require_once = true ) {
		$located = '';
		foreach ( (array) $template_names as $template_name ) {
			if ( !$template_name )
				continue;

			if ( file_exists( GS_ABSPATH . '/tmpl/' . $template_name ) ) {
				$located = GS_ABSPATH . '/tmpl/' . $template_name;
				break;
			} else 
			// default WP theme path
			if ( file_exists( TEMPLATEPATH . '/' . $template_name ) ) {
				$located = TEMPLATEPATH . '/' . $template_name;
				break;
			}
		}

		if ( $load && '' != $located )
			load_template( $located, $require_once );

		return $located;
	}
}