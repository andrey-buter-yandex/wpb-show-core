<?php 

/**
 * WPBC_Detect_Option_Tree
 */
class WPBC_Detect_Option_Tree
{
	function __construct()
	{
		if ( self::is_OT_loader_exists() )
			add_action( 'admin_notices', array( __CLASS__, 'admin_notice' ) );
	}

	/**
	 * Is exists OT loader
	 */
	public static function is_OT_loader_exists()
	{
		return class_exists( 'OT_Loader' );
	}

	/**
	 * Detec is OT-loader exists in theme
	 */
	public static function is_OT_in_theme()
	{
		$file = get_template_directory() .'/option-tree/ot-loader.php';

		return file_exists( $file );
	}

	/**
	 * Admin notice
	 */
	function admin_notice() {
		?>
			<div class="error">
				<p><strong>WPB Core:</strong> Plugin Option Tree not found. Please, download it <a href="https://wordpress.org/plugins/option-tree/">here</a> and setup to plugins.</p>
			</div>
		<?php 
	}
}