<?php
/**
 * General Sue Mathys theme handler
 */

class WPBC_General extends WPBC_Load_Section
{
	protected static $include_path;
	
	public static $section = 'general';

	public static $partials = array(
		
	);
}
WPBC_General::load();