<?php
/**
 * General Sue Mathys theme handler
 */

class WPBC_Shared extends WPBC_Load_Section
{
	protected static $include_path;
	
	public static $section = 'shared';

	public static $partials = array(
		'utils',
		// 'detect-option-tree.class',
	);

	static function on_load() 
	{
		// self::include_OT_settings();
	}

	private function include_OT_settings()
	{
		if ( !self::is_load_OT_local_settings() )
			return;

		$partials = array(
			'meta-boxes',
			'options'
		);

		static::include_partials( $partials );
	}

	/**
	 * Is load Option Tree settings 
	 */
	private static function is_load_OT_local_settings()
	{
		// declare on file detect-option-tree.class
		if ( !class_exists( 'WPBC_Detect_Option_Tree' ) )
			return false;

		if ( WPBC_Detect_Option_Tree::is_OT_in_theme() )
			return false;

		return true;
	}
}
WPBC_Shared::load();