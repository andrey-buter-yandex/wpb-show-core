<?php 


/**
 * Return current show stream
 * @return string
 *
 * DEPRECATED
 *
 * =========================
 * inc\module\show-stream\widget.php
 */
if ( ! function_exists( 'get_show_stream' ) ) {
	function get_show_stream() {
		return wpbc_get_global_stream();
	}
}

/**
 * DEPRECATED
 *
 * ====================
 * inc\module\show-schedule\schedule.php
 * inc\module\show-stream\stream.php
 */
if ( ! function_exists( 'set_show_stream' ) ) {
	function set_show_stream( $key = '' ) {
		return wpbc_set_global_stream( $key );
	}
}


/**
 * Return registered show streams
 *
 * DEPRECATED
 *
 * ==============
 * inc\module\show-schedule\template\page.php
 * inc\module\show-stream\widget.php
 * inc\meta-boxes.php
 */
if ( ! function_exists( 'get_show_streams' ) ) {
	function get_show_streams() {
		return wpbc_get_show_streams();
	}
}