<?php 


function wpbc_get_model_stream() {
	return WPBC_Model_Stream::get_instance();
}

/**
 * Return current global stream
 */
function wpbc_get_global_stream() {
	return wpbc_get_model_stream()->get_global_stream();
}


/**
 * Set current global stream
 */
function wpbc_set_global_stream( $key = '' ) {
	wpbc_get_model_stream()->set_global_stream( $key );
}

/**
 * Register new stream
 *
 * -------------------
 * Используется по аналогии register_post_type()
 */
function wpbc_register_stream( $stream ) {
	wpbc_get_model_stream()->register_stream( $stream );
}


/**
 * Return registered show streams
 *
 * ==============
 * inc\module\show-schedule\template\page.php
 * inc\module\show-stream\widget.php
 * inc\meta-boxes.php
 */
function wpbc_get_show_streams() {
	return wpbc_get_model_stream()->get_registered_streams();
}