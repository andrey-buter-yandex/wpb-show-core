<?php 

/**
 * Stream Object Model
 *
 * Pattern Singleton
 *
 * Хранит глобальный stream, зарегестрованные streams (которые отображаются в post_type show)
 */
class WPBC_Model_Stream
{
	private static $instance;

	private $streams = array();

	private $global_stream = '';

	private function __construct()
	{
		$this->set_default_streams();
	}

	/**
	 * Set default stream list
	 */
	private function set_default_streams()
	{
		$streams = array(
			array(
				'title' => 'General',
				'key'   => ''
			),
			array(
				'title' => 'XML',
				'key'   => 'xml'
			)
		);

		$this->streams = $streams;
	}

	/**
	 * Get list of available show streams
	 * @return array Available theme streams
	 *
	 * ================
	 * get_show_streams()
	 */
	public function get_registered_streams() 
	{
		$streams = $this->streams;

		// the filter 'itmwp_get_streams' is deprecated
		return apply_filters( 'itmwp_get_streams', $streams );
	}

	/**
	 * Register new stream
	 *
	 * ---------------------
	 * Регистрация нового stream глобально, вместо использования фильтра itmwp_get_streams
	 */
	public function register_stream( $stream )
	{
		$default = array(
			'title' => '',
			'key'   => ''
		);

		$stream = wp_parse_args( $default, $stream );

		$this->streams[] = $stream;
	}

	/**
	 * Set global current stream 
	 *
	 * ====================
	 * set_show_stream()
	 *
	 * Пока вижу, что set_stream устанавливается только 1 раз на Init или другой action
	 * Поэтому, для удобства stream лучше записывать в константу !!!!
	 * Но нужно поверить, устанавливается stream 1 раза или может быть случай с нескольким set stream
	 */
	public function set_global_stream( $key = '' )
	{
		$this->global_stream = $key;
	}

	/**
	 * Return global current stream
	 */
	public function get_global_stream()
	{
		return $this->global_stream;
	}

	/**
	 * Create Singleton pattern object
	 */
	public static function get_instance() {
		if ( empty( self::$instance ) )
			self::$instance = new self();

		return self::$instance;
	}
}