<?php 

/**
 * Handle every member aspect
 */

add_filter( 'wpbc_populate_meta_boxes', 'wpbc_populate_preroll_meta_boxes' );
function wpbc_populate_preroll_meta_boxes( $meta_boxes = array() ) {
	$prefix = 'preroll_';
	$meta_boxes[] = array(
		'id'       => 'preroll_meta',
		'title'    => __('Attributes', 'itmwp'),
		'pages'    => array('preroll'),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'label' => 'Audio',
				'id' => $prefix . 'audio',
				'type' => 'upload',
				'desc' => 'Upload Preroll audio file'
			),
		)
	);

	return $meta_boxes;
}