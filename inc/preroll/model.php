<?php

/**
 * Register show post type
 */
register_post_type( 'preroll',
	array(
		'labels' => array(
			'name'               => __( 'Audio Pre-rolls', 'itmwp2' ),
			'singular_name'      => __( 'Audio Pre-roll', 'itmwp2' ),
			'add_new'            => __( 'Add Audio Pre-roll', 'itmwp2' ),
			'add_new_item'       => __( 'Add New Audio Pre-roll', 'itmwp2' ),
			'edit'               => __( 'Edit', 'itmwp2' ),
			'edit_item'          => __( 'Edit Audio Pre-roll', 'itmwp2' ),
			'new_item'           => __( 'New Audio Pre-roll', 'itmwp2' ),
			'view'               => __( 'View Audio Pre-roll', 'itmwp2' ),
			'view_item'          => __( 'View Audio Pre-roll', 'itmwp2' ),
			'search_items'       => __( 'Search Pre-Rolls', 'itmwp2' ),
			'not_found'          => __( 'Not found', 'itmwp2' ),
			'not_found_in_trash' => __( 'No Audio Pre-rolls found in Trash', 'itmwp2' ),
		),
		'public'              => true,
		'exclude_from_search' => true,
		'menu_position'       => 20,
		'hierarchical'        => true,
		'taxonomies'    => array( 
			'podcast',
		),
		'has_archive'         => true,
		'query_var'           => true,
		'supports'            => array( 
			'title' 
		),
		'rewrite'             => array(
			'with_front' => false
		),
		'can_export'          => true,
	)
);


// add_action( 'admin_menu', array( $this, 'addVenueAndOrganizerEditor' ) );
/**
 * Adds the submenu items for editing the Venues and Organizers.
 * Used to be PRO only feature, but as of 3.0, it is part of Core.
 *
 * @since 2.0
 *
 * @return void


	смотри 787 - плагин the-events-calendar\lib\the-events-calendar.class.php


 */
// function addVenueAndOrganizerEditor() {
// 	add_submenu_page( '/edit.php?post_type=episode', __( 'Prerolls','wpbb' ), __( 'Prerolls','wpbb' ), 'edit_tribe_venues', 'edit.php?post_type=preroll' );
// }