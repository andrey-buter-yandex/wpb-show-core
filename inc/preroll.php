<?php
/**
 * Handle NUK Products
 */

class WPBC_Model_Preroll extends WPBC_Load_Section
{
	protected static $include_path;
	
	public static $section = 'preroll';

	public static $partials = array(
		'model',
		'meta'
	);
}
WPBC_Model_Preroll::load();