<?php
/**
 * Handle NUK Products
 */

class WPBC_Model_Episode extends WPBC_Load_Section
{
	protected static $include_path;
	
	public static $section = 'episode';

	public static $partials = array(
		'model',
		'meta',
		'admin/on-save-post.class',
		'lib/episodes-query.class',
		'lib/get-episode-content.class',
		'lib/episode-thumbnail.class',
		'public-lib',
		'shorcodes',
		'deprecated'
	);

	// public static function on_load() 
	// {
	// 	add_action( 'init', array( __CLASS__, 'load_deprecated' ), 20 );
	// }
}
WPBC_Model_Episode::load();