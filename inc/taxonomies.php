<?php 

/**
 * Handle NUK Products
 */

class WPBC_Model_Taxonomies extends WPBC_Load_Section
{
	protected static $include_path;
	
	public static $section = 'taxonomies';

	public static $partials = array(
		'model',
		'meta',
		'admin/walker-podcast-category-dropdown.class',
		'admin/episodes-list-table.class',
		'public',
		'shortcodes',
		'deprecated'
	);
}
WPBC_Model_Taxonomies::load();