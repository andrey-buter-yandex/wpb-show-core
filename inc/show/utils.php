<?php 

/**
 * Define show stream prefix
 */
function wpbc_get_show_stream_prefix() {
	$current_stream = wpbc_get_global_stream();

	$prefix = 'show_';

	if ( empty( $current_stream ) )
		return $prefix;

	return $prefix . $current_stream . '_';
}