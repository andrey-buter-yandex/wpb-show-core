<?php 


/**
 * Fielters for front end functions
 */
class WPBC_Show_Filters_Front_End
{
	/**
	 * Add actions and filters
	 */
	public static function load_actions() 
	{
		if ( is_admin() )
			return;

		// filter get_permalink()
		add_filter( 'post_type_link', array( __CLASS__, 'post_type_link' ), 10, 2 );
	}

	/**
	 * Use custrom url for shows if it's not empty
	 * Filter get_permalink()
	 *
	 * @param  string $permalink Current Show Permalink
	 * @param  object $post      WP_Post Object
	 * @return string            Filtered permalink
	 */
	public function post_type_link( $permalink, $post ) 
	{
		if ( 'show' != $post->post_type )
			return $permalink;

		$meta_key = wpbc_get_show_stream_prefix() .'url';

		$url = get_post_meta( $post->ID, $meta_key, true );

		if ( $url )
			return $url;

		return $permalink;
	}
}
WPBC_Show_Filters_Front_End::load_actions();