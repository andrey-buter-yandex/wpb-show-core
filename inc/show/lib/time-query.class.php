<?php

/**
 * Get show meta data functions library
 */
class WPBC_Show_Time_Query
{
	/**
	 * Output show time
	 * @param  string $time_key Retrieve time by key start|end|range.
	 * @return void
	 * 
	 * ===================
	 * meta\show.php
	 * section\schedule-now.php
	 */
	public static function the_show_time( $time_key = 'range', $format = '', $range_divider = '' ) 
	{
		if ( empty( $format ) )
			$format = get_option( 'time_format' );

		echo self::get_the_show_time( get_the_ID(), $time_key, $format, $range_divider );
	}

	/**
	 * Get show time according to passed key
	 * @param  int $post_id     Show ID.
	 * @param  string $time_key Time key.
	 * @return string
	 *
	 * ========================
	 * inc\module\show-schedule\schedule.php
	 * inc\module\show-schedule\template\feed.php
	 */
	public static function get_the_show_time( $post_id, $time_key, $format, $range_divider = ' - ' ) 
	{
		$time = '';

		switch ( $time_key ) {
			case 'start':
				$time  = date( $format, self::_get_meta_show_time( $post_id, 'time_start' ) );
				break;
			case 'end':
				$time  = date( $format, self::_get_meta_show_time( $post_id, 'time_end' ) );
				break;
			case 'range':
				$start = date( $format, self::_get_meta_show_time( $post_id, 'time_start' ) );
				$end   = date( $format, self::_get_meta_show_time( $post_id, 'time_end' ) );
				$time  = $start .' - '. $end;
				break;
			// No default case
		}

		return $time;
	}

	/**
	 * Get show time from post meta
	 *
	 * used by self::get_the_show_time()
	 */
	private static function _get_meta_show_time( $post_id, $meta_key )
	{
		$prefix = wpbc_get_show_stream_prefix();

		return self::format_time( get_post_meta( $post_id, $prefix . $meta_key, true ) );
	}

	/**
	 * Convert passed time string to timestamp
	 * @param  string $time Time as sting hh:mm
	 * @return string       Timestamp or empty string if nothing was passed
	 *
	 * =======================
	 * get_the_show_time()
	 */
	private static function format_time( $time = '' ) {
		if ( empty( $time ) )
			return $time;

		$time_arr = explode( ':', $time );

		if ( count( $time_arr ) > 1 )
			$time = mktime( $time_arr[0], $time_arr[1] );

		return $time;
	}

	/**
	 * Update show time
	 * @param int $post_id       Post show id.
	 * @param array  $time_start Show Start time
	 * @param array  $time_end   Show end time
	 *
	 * =========================
	 * inc\module\show-schedule\schedule.php
	 */
	public static function set_show_time( $post_id, $start = array(), $end = array(), $clear = false ) 
	{
		$prefix = wpbc_get_show_stream_prefix();

		// Maybe clear time meta for show
		if ( $clear ) {
			delete_post_meta( $post_id, $prefix . 'time_start' );
			delete_post_meta( $post_id, $prefix . 'time_end' );
		}

		if ( isset( $start ) AND isset( $start['hour'] ) AND isset( $start['minute'] ) )
			update_post_meta( $post_id, $prefix . 'time_start', $start['hour'] .':'. $start['minute'] );

		if ( isset( $end )AND isset( $end['hour'] ) AND isset( $end['minute'] ) )
			update_post_meta( $post_id, $prefix . 'time_end', $end['hour'] .':'. $end['minute'] );

		return true;
	}

	/**
	 * Render show days 
	 *
	 * =========================
	 * meta\show.php
	 */
	public static function the_show_days( $separator = ', ' ) 
	{
		echo implode( $separator, self::get_show_days( get_the_ID(), 'human' ) );
	}

	/**
	 * Get checked show days in either 
	 * human (array of day strings) or
	 * machine (array of ints starting from Sunday) formats
	 * 
	 * @param  int  $post_id    Show ID.
	 * @param  boolean $human   Return in human format or as array
	 * @return array     
	 *
	 * ==========================
	 * inc\module\show-schedule\schedule.php
	 * inc\module\show-schedule\template\feed.php
	 */
	public static function get_show_days( $post_id, $format = 'human' ) 
	{
		$prefix = wpbc_get_show_stream_prefix();

		$days = get_post_meta( $post_id, $prefix . 'days', true );

		if ( empty( $days ) ) {
			return array();
		}

		// Change numbers to human days
		if ( 'human' == $format ) {
			foreach ( $days as $key => $day ) {

				// Small hack to retrieve translated day by timestamp for current day
				$days[ $key ] = date_i18n( 'l', strtotime( 'Sunday +'. $day .' days' ) );
			}
		}

		return $days;
	}

	/**
	 * Update show days schedule
	 * @param int $post_id  Post id
	 * @param array  $days  Show days
	 *
	 * =======================
	 * inc\module\show-schedule\schedule.php
	 */
	public static function set_show_days( $post_id, $temp_days = array(), $clear = false ) 
	{
		$prefix = wpbc_get_show_stream_prefix();

		if ( $clear )
			delete_post_meta( $post_id, $prefix . 'days' );

		if ( empty( $temp_days ) )
			return false;

		// Prepare days format
		$days = array();
		foreach ( $temp_days as $key => $value ) {
			$days[ absint($value) ] = ''. $value;
		}

		return update_post_meta( $post_id, $prefix . 'days', $days, $prev_value = '' );
	}
}