<?php 

/**
 * WPBC_Show_Query functions Library
 */
class WPBC_Show_Query_Library
{
	/**
	 * Get current/next show
	 */
	public static function get_show( $type = '', $return = 'array' )
	{
		switch ( $type ) {
			case 'current':
				$show = self::get_current( $return );
				break;
			case 'next':
				$show = self::get_next( $return );
				break;
			// No default case
		}

		return $show;
	}

	/**
	 * Get currently on air show
	 * @return mixed (null|object) either false if no show is currently on air or show post object
	 */
	private static function get_current( $return = '' ) 
	{
		$current_time = current_time( 'timestamp' );

		$args = array(
			'weekday'        => date( 'w', $current_time ),
			'time_query'     => array(
				array(
					'key'       => 'time_start',
					'compare'   => '<',
					'value'     => date( 'H:i', $current_time )
				),
				array(
					'key'       => 'time_end',
					'compare'   => '>',
					'value'     => date( 'H:i', $current_time )
				)
			),
			'posts_per_page' => 1
		);

		return self::get_posts( $args, $return );
	}

	/**
	 * Get closest upcoming show
	 * @return mixed
	 *
	 * @todo Find next schedule ids with single db request
	 */
	private static function get_next( $return, $extra = array() ) 
	{
		$current_time = current_time( 'timestamp' );

		$default = array(
			'weekday'        => date( 'w', $current_time ),
			'time_query'     => array(
				array(
					'key'       => 'time_start',
					'compare'   => '>',
					'value'     => date( 'H:i', $current_time )
				)
			),
			'posts_per_page' => 1,
			'orderby'        => 'post__in'
		);

		$args = wp_parse_args( $extra, $default );

		// Make this check in single request
		for ( $i = 0; $i < 7; $i++ ) {
			$args['weekday'] = date( 'w', strtotime( '+ '. $i .' day', $current_time ) );

			$next = self::get_posts( $args, $return );

			if ( empty( $next ) )
				continue;

			// exists WP_Quert->have_posts()
			if ( method_exists( $next, 'have_posts' ) AND $next->have_posts() )
				break;

			if ( isset( $next->ID ) )
				break;
		}

		return $next;
	}

	/**
	 * Get broadcast according to passed query
	 * @param  array  $args broadcast query args
	 * @return mixed (object|false)   WP_Query object or false if there are no shows filtered by prepending meta queries
	 */
	private static function get_posts( $args = array(), $return = 'array' ) 
	{
		$args['post_type']   = 'show';
		// $args['post_status'] = 'publish';

		// Meta queries
		if ( isset( $args['weekday'] ) ) {

			// Convert weekdays to array
			if ( !is_array( $args['weekday'] ) )
				$args['weekday'] = array( $args['weekday'] );

			$args['post__in'] = self::weekday_query( $args['weekday'] );
		}

		if ( isset( $args['time_query'] ) ) {

			$time_ids = self::time_query( $args['time_query'] );

			// We need only intersections if there are already some posts to include
			if ( isset( $args['post__in'] ) AND
				 !( $args['post__in'] = array_intersect( $time_ids, $args['post__in'] ) )
			) {
				$args['post__in'] = array( -1 );
			}
		}

		// If we need only one post than post object will be returned
		if ( isset( $args['posts_per_page'] ) AND 1 == $args['posts_per_page'] AND 'array' == $return ) {
			$shows = get_posts( $args );

			if ( ! empty( $shows ) ) 
				$shows = array_shift( $shows );

		} else {
			$shows = new WP_Query( $args );
		}

		return $shows;
	}

	/**
	 * Get array of show ids by weekdays
	 * @param  array  $weekdays array of weekdays
	 * @return array            show ids
	 */
	private static function weekday_query( $weekdays = array() ) 
	{
		global $wpdb;

		$prefix = wpbc_get_show_stream_prefix();

		$sql = "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '{$prefix}days'";
		$shows = $wpdb->get_results( $sql, OBJECT_K );

		$ids = array();

		if ( !empty( $shows ) ) {

			foreach ( $shows as $show ) {
				$days = maybe_unserialize( $show->meta_value );

				if ( array_intersect( $days, $weekdays ) )
					$ids[] = absint( $show->post_id );
			}

		}

		if ( empty( $ids ) )
			$ids = array( -1 );

		return $ids;
	}

	/**
	 * Get show ids by specific time queyr
	 * @param  array  $time_query time query
	 * @return array              show ids
	 */
	private static function time_query( $time_query = array() ) 
	{
		global $wpdb;

		$prefix = wpbc_get_show_stream_prefix();

		// Collect where query part
		$where = '';

		foreach ( $time_query as $query ) {
			if ( !in_array( $query['key'], array( 'time_start', 'time_end' ) ) )
				continue;

			$key     = esc_sql( $query['key'] );
			$compare = esc_sql( $query['compare'] );
			$value   = esc_sql( $query['value'] );

			$add_where = "( meta_key = '{$prefix}{$key}' AND meta_value $compare '$value' )";

			$where .= empty( $where ) ? $add_where : ' OR ' . $add_where;
		}

		if ( empty( $where ) )
			return array( -1 );

		$sql = "SELECT post_id FROM $wpdb->postmeta WHERE $where ORDER BY meta_value ASC";

		$ids = $wpdb->get_col( $sql );

		if ( count( $time_query ) )
			$ids = self::get_duplicates( $ids, count( $time_query ) );

		if ( empty( $ids ) )
			$ids = array( -1 );

		return $ids;
	}

	/**
	 * Find duplicates depending on the number of duplicates
	 * @param array of values to filter
	 * @return array of keys from initial array that passed the check
	 */
	private static function get_duplicates( $array, $count ) 
	{
		$array = array_count_values( $array );

		// Leave values which 'count' equals passed $count
		foreach ( $array as $key => $value ) {
			if ( $value != $count )
				unset( $array[$key] );
		}
		return array_keys( $array );
	}

	/**
	 * Get show by podcast taxonomy directly or by podcast connected to the post
	 * @param  string|int|object  $podcast Term id or name or object itself
	 * @param  integer $post               Optional. If no podcast passed it will be retrieved from passed post
	 * @return object|null
	 */
	public static function get_by_podcast( $podcast = '', $post = 0 ) 
	{
		if ( empty( $podcast ) AND empty( $post ) )
			return false;

		// If no podcast is passed we need to retrieve podcast from passed post
		if ( empty( $podcast ) ) {
			$post = get_post( $post );

			if ( $post AND in_array( $post->post_type, array( 'episode', 'post' ) ) !== false )
				$podcast = wp_get_object_terms( $post->ID, 'podcast', array( 'fields' => 'ids' ) );

			unset( $post );
		}

		if ( empty( $podcast ) )
			return false;

		// We are dealing with single podcast term
		if ( is_array( $podcast ) ) {
			$podcast = array_shift( $podcast );

			if ( absint( $podcast ) > 0 )
				$podcast = absint( $podcast );
		}

		// Podcast can be passed as an object, as single term name or as term id
		if ( is_object( $podcast ) ) {
			$field = 'id';
			$terms = $podcast->term_id;
		} elseif ( is_int( $podcast ) ) {
			$field = 'id';
			$terms = $podcast;
		} else {
			$field = 'name';
			$terms = $podcast;
		}

		return self::get_posts(array(
			'posts_per_page' => 1,
			'tax_query'      => array(
				array(
					'taxonomy' => 'podcast',
					'field'    => $field,
					'terms'    => $terms
				)
			)
		));
	}
}