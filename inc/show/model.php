<?php
/**
 * Register show post type
 */

register_post_type( 'show',
	array(
		'labels'        => array(
			'name'               => __( 'Shows', 'itmwp2' ),
			'singular_name'      => __( 'Show', 'itmwp2' ),
			'add_new'            => __( 'Add Show', 'itmwp2' ),
			'add_new_item'       => __( 'Add New Show', 'itmwp2' ),
			'edit'               => __( 'Edit', 'itmwp2' ),
			'edit_item'          => __( 'Edit Show', 'itmwp2' ),
			'new_item'           => __( 'New Show', 'itmwp2' ),
			'view'               => __( 'View Shows', 'itmwp2' ),
			'view_item'          => __( 'View Show', 'itmwp2' ),
			'search_items'       => __( 'Search Shows', 'itmwp2' ),
			'not_found'          => __( 'Not found', 'itmwp2' ),
			'not_found_in_trash' => __( 'No Shows found in Trash', 'itmwp2' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => true,
		'taxonomies'    => array( 
			'podcast',
			'collection'
		),
		'has_archive'   => true,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			'editor', 
			'thumbnail', 
			'custom-fields' 
		),
		'rewrite'       => array(
			'with_front'  => false
		),
		'can_export'    => true,
	)
);