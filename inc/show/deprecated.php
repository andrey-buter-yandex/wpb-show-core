<?php 

/**
 * Get show by available type
 * @param  string $type   Retrieval type, current|next
 * @param  string $return Return result as array of WP_Query object
 * @return array|object
 *
 * ====================
 * section\schedule-now.php
 */
if ( !function_exists( 'get_show' ) ) {
	function get_show( $type = '', $return = 'array' ) {
		return wpbc_get_show( $type, $return );
	}
}


/**
 * Output show time
 * @param  string $time_key Retrieve time by key start|end|range.
 * @return void
 * 
 * ===================
 * meta\show.php
 * section\schedule-now.php
 */
if ( !function_exists( 'the_show_time' ) ) {
	function the_show_time( $time_key = 'range', $format = '', $range_divider = '' ) {
		wpbc_the_show_time( $time_key, $format, $range_divider );
	}
}
	

/**
 * Get show time according to passed key
 * @param  int $post_id     Show ID.
 * @param  string $time_key Time key.
 * @return string
 *
 * ========================
 * inc\module\show-schedule\schedule.php
 * inc\module\show-schedule\template\feed.php
 */
if ( !function_exists( 'get_the_show_time' ) ) {
	function get_the_show_time( $post_id, $time_key, $format, $range_divider = ' - ' ) {
		return wpbc_get_the_show_time( $post_id, $time_key, $format, $range_divider );
	}
}
	

/**
 * Update show time
 * @param int $post_id       Post show id.
 * @param array  $time_start Show Start time
 * @param array  $time_end   Show end time
 *
 * =========================
 * inc\module\show-schedule\schedule.php
 */
if ( !function_exists( 'set_show_time' ) ) {
	function set_show_time( $post_id, $start = array(), $end = array(), $clear = false ) {
		return wpbc_set_show_time( $post_id, $start, $end, $clear );
	}
}
	

/**
 * Render show days 
 *
 * =========================
 * meta\show.php
 */
if ( !function_exists( 'the_show_days' ) ) {
	function the_show_days( $separator = ', ' ) {
		wpbc_the_show_days( $separator );
	}
}
	

/**
 * Get checked show days in either 
 * human (array of day strings) or
 * machine (array of ints starting from Sunday) formats
 * 
 * @param  int  $post_id    Show ID.
 * @param  boolean $human   Return in human format or as array
 * @return array     
 *
 * ==========================
 * inc\module\show-schedule\schedule.php
 * inc\module\show-schedule\template\feed.php
 */
if ( !function_exists( 'get_show_days' ) ) {
	function get_show_days( $post_id, $format = 'human' ) {
		return wpbc_get_show_days( $post_id, $format );
	}
}


/**
 * Update show days schedule
 * @param int $post_id  Post id
 * @param array  $days  Show days
 *
 * =======================
 * inc\module\show-schedule\schedule.php
 */
if ( !function_exists( 'set_show_days' ) ) {
	function set_show_days( $post_id, $temp_days = array(), $clear = false ) {
		return wpbc_set_show_days( $post_id, $temp_days, $clear );
	}
}
