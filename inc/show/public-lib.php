<?php
/**
 * This File holds episode class and connected public functions
 */

/* = Depends on class WPBC_Show_Query_Library()
===================================================================== */

/**
 * Get show by available type
 * @param  string $type   Retrieval type, current|next
 * @param  string $return Return result as array of WP_Query object
 * @return array|object
 *
 * ====================
 *
 * Depends on class WPBC_Show_Query_Library
 *
 * ====================
 * section\schedule-now.php
 */
function wpbc_get_show( $type = '', $return = 'array' ) {
	return WPBC_Show_Query_Library::get_show( $type, $return );
}

/**
 * Get show by available type
 * @param  string $type   Retrieval type, current|next
 * @param  string $return Return result as array of WP_Query object
 * @return array|object
 *
 * ====================
 *
 * Depends on class WPBC_Show_Query_Library
 *
 * ====================
 * section\schedule-now.php
 */
function wpbc_get_show_by_podcast( $podcast = '', $post = 0 ) {
	return WPBC_Show_Query_Library::get_by_podcast( $podcast, $post );
}



/* = Depends on class WPBC_Show_Time_Query()
===================================================================== */

/**
 * Output show time
 * @param  string $time_key Retrieve time by key start|end|range.
 * @return void
 *
 * ====================
 *
 * Depends on class WPBC_Show_Time_Query
 * 
 * ===================
 * meta\show.php
 * section\schedule-now.php
 */
function wpbc_the_show_time( $time_key = 'range', $format = '', $range_divider = '' ) {
	WPBC_Show_Time_Query::the_show_time( $time_key, $format, $range_divider );
}

/**
 * Get show time according to passed key
 * @param  int $post_id     Show ID.
 * @param  string $time_key Time key.
 * @return string
 *
 * ====================
 *
 * Depends on class WPBC_Show_Time_Query
 *
 * ========================
 * inc\module\show-schedule\schedule.php
 * inc\module\show-schedule\template\feed.php
 */
function wpbc_get_the_show_time( $post_id, $time_key, $format, $range_divider = ' - ' ) {
	return WPBC_Show_Time_Query::get_the_show_time( $post_id, $time_key, $format, $range_divider );
}

/**
 * Update show time
 * @param int $post_id       Post show id.
 * @param array  $time_start Show Start time
 * @param array  $time_end   Show end time
 *
 * ====================
 *
 * Depends on class WPBC_Show_Time_Query
 *
 * =========================
 * inc\module\show-schedule\schedule.php
 */
function wpbc_set_show_time( $post_id, $start = array(), $end = array(), $clear = false ) {
	return WPBC_Show_Time_Query::set_show_time( $post_id, $start, $end, $clear );
}

/**
 * Render show days 
 *
 * ====================
 *
 * Depends on class WPBC_Show_Time_Query
 *
 * =========================
 * meta\show.php
 */
function wpbc_the_show_days( $separator = ', ' ) {
	WPBC_Show_Time_Query::the_show_days( $separator );
}

/**
 * Get checked show days in either 
 * human (array of day strings) or
 * machine (array of ints starting from Sunday) formats
 *
 * ====================
 *
 * Depends on class WPBC_Show_Time_Query
 * 
 * @param  int  $post_id    Show ID.
 * @param  boolean $human   Return in human format or as array
 * @return array     
 *
 * ==========================
 * inc\module\show-schedule\schedule.php
 * inc\module\show-schedule\template\feed.php
 */
function wpbc_get_show_days( $post_id, $format = 'human' ) {
	return WPBC_Show_Time_Query::get_show_days( $post_id, $format );
}

/**
 * Update show days schedule
 * @param int $post_id  Post id
 * @param array  $days  Show days
 *
 * ====================
 *
 * Depends on class WPBC_Show_Time_Query
 *
 * =======================
 * inc\module\show-schedule\schedule.php
 */
function wpbc_set_show_days( $post_id, $temp_days = array(), $clear = false ) {
	return WPBC_Show_Time_Query::set_show_days( $post_id, $temp_days, $clear );
}