<?php 

/**
 * Handle every member aspect
 */

add_filter( 'wpbc_populate_meta_boxes', 'wpbc_populate_show_meta_boxes' );
function wpbc_populate_show_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'show';

	$prefix = $post_type .'_';

	$meta_boxes[] = array(
		'id'       => $prefix .'social_meta',
		'title'    => __( 'Social', 'itmwp' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'id'       => $prefix . 'social',
				'label'    => '',
				'type'     => 'list-item',
				'settings' => array( 
					array(
						'id'     => 'url',
						'label'  => __( 'Url', 'itmwp' ),
						'type'   => 'text',
					),
					array(
						'id'     => 'icon',
						'label'  => __( 'Icon', 'itmwp' ),
						'type'   => 'upload',
					)
				)
			),
		)
	);

	return $meta_boxes;
}


add_filter( 'wpbc_populate_meta_boxes', 'wpbc_populate_show_stream_meta_boxes' );
function wpbc_populate_show_stream_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'show';

	$prefix = $post_type .'_';

	$streams = wpbc_get_show_streams();

	if ( !$streams )
		return;

	foreach ( $streams as $stream ) {

		// Default no stream prefix for main stream
		$prefix = empty( $stream['key'] ) ? $prefix : $prefix . $stream['key'] . '_';

		$meta_boxes[] = array(
			'id'       => 'show_meta' . $stream['key'],
			'title'    => sprintf( __( '%s Stream Attributes', 'itmwp2' ), $stream['title'] ),
			'pages'    => array( $post_type ),
			'context'  => 'normal',
			'priority' => 'high',
			'fields'   => array(
				array(
					'label'   => 'Weekdays',
					'id'      => $prefix . 'days',
					'type'    => 'checkbox',
					'choices' => array(
						array(
							'label' => 'Sunday',
							'value' => '0'
						),
						array(
							'label' => 'Monday',
							'value' => '1'
						),
						array(
							'label' => 'Tuesday',
							'value' => '2'
						),
						array(
							'label' => 'Wednesday',
							'value' => '3'
						),
						array(
							'label' => 'Thursday',
							'value' => '4'
						),
						array(
							'label' => 'Friday',
							'value' => '5'
						),
						array(
							'label' => 'Saturday',
							'value' => '6'
						),
					),
					'desc' => 'Show schedule'
				),
				array(
					'label'    => 'Time start',
					'id'       => $prefix . 'time_start',
					'type'     => 'datetimepicker',
					'desc'     => 'Select show start time',
					'settings' => array(
						'type'       => 'time',
						'timeFormat' => 'HH:mm',
						'showSecond' => false
					)
				),
				array(
					'label'    => 'Time end',
					'id'       => $prefix . 'time_end',
					'type'     => 'datetimepicker',
					'desc'     => 'Select show end time',
					'settings' => array(
						'type'       => 'time',
						'timeFormat' => 'HH:mm',
						'showSecond' => false
					)
				),
				array(
					'label' => 'Targeting url',
					'id'    => $prefix . 'url',
					'type'  => 'text',
					'desc'  => 'Set custom destination url'
				),
			)
		);
	}

	return $meta_boxes;
}	