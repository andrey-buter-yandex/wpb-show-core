<?php
/**
 * General Sue Mathys theme handler
 *
 * Независимый от других блоков, блок stream
 * При загрузке определяется глобальный stream
 * Прикрепляется к show
 */
class WPBC_Stream extends WPBC_Load_Section
{
	protected static $include_path;
	
	public static $section = 'stream';

	public static $partials = array(
		'model.class',
		'public',
		'deprecated'
	);

	// public static function on_load() 
	// {
	// 	add_action( 'init', array( __CLASS__, 'load_deprecated' ), 20 );
	// }
}
WPBC_Stream::load();