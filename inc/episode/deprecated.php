<?php 

/**
 * ==============================
 * content.php
 * front\slider.php
 * widget\recent-gallery.php
 * widget\recent-summary.php
 * widget\recent-thumbnail-content-first.php
 * widget\recent-thumbnail-summary.php
 * widget\recent-thumbnail.php
 * widget\recent.php
 */
if ( !function_exists( 'the_post_episode' ) ) {
	function the_post_episode() {
		wpbc_the_post_episode();
	}
}

if ( !function_exists( 'get_post_episode' ) ) {
	function get_post_episode( $post_id = null ) {
		return wpbc_get_post_episode( $post_id );
	}
}


/**
 * Retrieve audio samples from shortcode $atts
 * and pass to jPlayer global instance.
 * @uses function get_player defined in player module
 * 
 * @param  array  $atts shortcode attributes
 * @return string       html string
 *
 * ==============================
 * NOT FOUND IN THE THEME
 *
 * Но! $GLOBALS['episodes_query'] используется в
 * section\episodes.php
 * section\feed-schedule.php
 */
if ( false ) {
	function episodes_shortcode( $atts = array() ) 
	{
		extract( shortcode_atts( array(
			'id' => get_the_ID(),
		), $atts ) );

		$show_id = absint( $id );
		$GLOBALS['episodes_query'] = self::get_by_show( $show_id, OBJECT );

		ob_start();
		get_theme_part( 'section', 'episodes' );
		unset( $GLOBALS['episodes_query'] );

		return ob_get_clean();
	}
}


/**
 * Get episodes by 
 * @param  string $get_by [description]
 * @return [type]         [description]
 *
 * ==============================
 *
 * depends on class WPBC_Episodes_Query
 *
 * ===============================
 * inc\module\episode-stats\class\class-player-stats-list-table.php
 * inc\module\episode-stats\class\class-podcast-stats-list-table.php
 */
if ( !function_exists( 'get_episodes_by' ) ) {
	function get_episodes_by( $field, $value, $return = ARRAY_N ) {
		return wpbc_get_episodes_by( $field, $value, $return );
	}
}
	