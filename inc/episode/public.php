<?php 



if ( !is_admin() )
	add_filter( 'wp_trim_excerpt', 'wpbc_episode_custom_excerpt_more', 100 );
/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @param string $output post content or excerpt
 * @return string post excerpt with more link
 */
function wpbc_episode_custom_excerpt_more( $output ) {

	// deprecated version
	if ( function_exists( 'itmwp2_continue_reading_link' ) )
		return $output;

	if ( 'episode' != get_post_type() )
		return $output;

	$continue_reading = ' <a class="continue-reading" href="'. esc_url( get_permalink() ) . '">' . __( 'Continue Reading &raquo;', 'itmwp2' ) .'</a>';

	return $output . $continue_reading;
}