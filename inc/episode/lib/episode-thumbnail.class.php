<?php 

/**
 * Load Episode Thumbnail
 */
class WPBC_Episode_Thumbnail
{	
	/**
	 * Add actions and filters
	 */
	public static function load_actions() 
	{
		if ( is_admin() )
			return;

		// Show thumbnail is default for connected posts and episodes
		add_filter( 'post_thumbnail_html', array( __CLASS__, 'post_thumbnail_html' ), 10, 5 );
	}

	/**
	 * Use show thumbnail as default featured image for connected posts & episodes
	 *
	 * @param  string $html              Html imeage or empty string
	 * @param  int $post_id              Current post ID
	 * @param  int $post_thumbnail_id    Current post thumbnail id
	 * @param  string $size              Image size
	 * @param  string|array $attr        Optional. Query string or array of attributes.
	 * @return string                    Html with image or empty string
	 */
	function post_thumbnail_html( $html = '', $post_id, $post_thumbnail_id, $size = 'thumbnail', $attr = '' ) 
	{
		$post = get_post( $post_id );

		if ( empty( $html ) && 'episode' == $post->post_type ) {

			// First we are searching for podcast thumbnail
			$podcast = wp_get_object_terms( $post_id, 'podcast', array( 'fields' => 'ids' ) );

			if ( !empty( $podcast ) && !is_wp_error( $podcast ) ) {

				// We are allways dealing only with single podcast
				$podcast = array_shift( $podcast );

				if ( $thumb_id = get_theme_term_thumbnail_id( absint( $podcast ), 'podcast' ) ) {
					$html = wp_get_attachment_image( $thumb_id, $size, false, $attr );
				}

				// No podcast thumbanail was found
				if ( empty( $html ) AND 
					 function_exists( 'wpbc_get_show_by_podcast' ) AND
					 $show = wpbc_get_show_by_podcast( '', $post ) 
				) {
					$html = get_the_post_thumbnail( $show->ID, $size, $attr );
				}
			}
		}
		return $html;
	}
}
WPBC_Episode_Thumbnail::load_actions();