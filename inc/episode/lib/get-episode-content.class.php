<?php 

/**
 * WPBC_Get_Episode
 */
class WPBC_Get_Episode_Content
{
	/**
	 *
	 * ====================
	 * get_post_episode()
	 */
	public static function get_post_episode( $post_id = null )
	{
		global $post;

		if ( !$post_id )
			return;

		if ( $post_id != get_the_ID() )
			$post = get_post( $post_id );

		// Current post can be episode itself
		if ( 'episode' == $post->post_type )
			return self::get_content( array( 'id' => $post->ID ) );

		if ( !has_shortcode( $post->post_content, 'episode' ) )
			return;

		// Search for episode shortcode
		$shortcode = self::get_shortcodes( $post->post_content, true, true );

		return do_shortcode( $shortcode );
	}


	/**
	 * Retrieve audio samples from shortcode $atts
	 * and pass to jPlayer global instance.
	 * @uses function get_player defined in player module
	 * 
	 * @param  array  $atts shortcode attributes
	 * @return string       html string
	 *
	 * ==========================
	 *
	 * used by 
	 * - function episode_shortcode()
	 * - self::get_post_episode()
	 *
	 * ==========================
	 * 
	 */
	public static function get_content( $atts = array() ) 
	{
		$default = array(
			'id' => 0,
		);

		$atts = shortcode_atts( $default, $atts );

		$post_id = absint( $atts['id'] );

		// Episode id
		if ( !$post_id )
			$post_id = get_the_ID();

		if ( is_feed() )
			return self::get_feed_episode( $post_id );

		return self::get_content_episode( $post_id );
	}

	/**
	 * Feed episode content
	 * @param  int $episode_id Episode id
	 * @return string          Html link with episode title and mp3
	 *
	 * =======================
	 * self::get_content()
	 */
	private static function get_feed_episode( $post_id ) 
	{
		$content = '';

		$episode_audio = get_post_meta( $post_id, 'episode_audio', true );

		if ( $episode_audio )
			$content = '<a href="'. $episode_audio .'">'. get_the_title( $post_id ) .'</a>';

		return $content;
	}

	/**
	 * Front epsiode ouput
	 * @param  int $post_id Episodde id
	 * @return string  Episode player html
	 *
	 * =======================
	 * self::get_content()
	 */
	private static function get_content_episode( $post_id ) 
	{
		$content = '';

		if ( !$post_id )
			return $content;

		$episode_audio = get_post_meta( $post_id, 'episode_audio', true );

		if ( !$episode_audio )
			return $content;

		$audio[] = array(
			'id'    => $post_id,
			'mp3'   => $episode_audio,
			'title' => get_the_title( $post_id )
		);

		// Check autoplay option
		$autoplay = ( is_single() AND isset( $atts['autoplay'] ) ) ? absint( $atts['autoplay'] ) : 0;

		// By default preroll_id is set as episode meta
		$preroll_id = absint( get_post_meta( $post_id, 'episode_preroll', true ) );

		// If no preroll is set, try to find random preroll from the same podcast category
		if ( 0 == $preroll_id ) {

			// Get episode podcast category
			$podcast = wp_get_object_terms( $post_id, 'podcast', array( 'fields' => 'ids' ) );

			if ( !is_wp_error( $podcast ) AND !empty( $podcast ) ) {
				$preroll_args = array(
					'post_type'      => 'preroll',
					'posts_per_page' => 1,
					'tax_query'      => array(
						array(
							'taxonomy'  => 'podcast',
							'field'     => 'id',
							'terms'     => $podcast
						)
					),

					// to-do get rid of this param and make it another way to speed up
					'orderby'        => 'rand'
				);

				$preroll = get_posts( $preroll_args );

				if ( !empty( $preroll ) ) {
					$preroll = array_shift( $preroll );
					$preroll_id = $preroll->ID;
				}

			} // Random preroll request
		} // Empty episode preroll meta

		$preroll_audio = get_post_meta( $preroll_id, 'preroll_audio', true );

		if ( $preroll_audio ) {
			array_unshift( $audio, array(
				'id'      => $preroll_id,
				'mp3'     => $preroll_audio,
				'title'   => get_the_title( $preroll_id ),
				'preroll' => true
			));
		}

		$content = get_player( $audio, array( 'key' => 'episode', 'autoplay' => $autoplay ) );

		return $content;
	}

	/**
	 * Search passed content for episode shortcode
	 * @param  string  $content 
	 * @param array $query additional shortcode query
	 * @return mixed (bool|string)          retrun string containing shortcode or false if none was found
	 *
	 * ==========================================
	 * self::get_post_episode()
	 *
	 * переделать на get_shortcodes - получать шорткоды из контента. 
	 * А проверять наличие шорткодов дефолтной функцией ВП has_shortcode()
	 *
	 */
	private static function get_shortcodes( $content = '', $single = false, $sticky = false ) 
	{
		if ( empty( $content ) )
			return false;

		// We need pattern with our shortcode
		$temp_tags = $GLOBALS['shortcode_tags'];
		$GLOBALS['shortcode_tags'] = array( 'episode' => '' );
		$pattern = get_shortcode_regex();
		$GLOBALS['shortcode_tags'] = $temp_tags;

		preg_match_all( "/$pattern/s", $content, $shortcodes );

		$shortcodes = empty( $shortcodes ) ? array() : $shortcodes[0];

		if ( empty( $shortcodes ) )
			return false;

		if ( $sticky ) {
			foreach ($shortcodes as $key => $shortcode ) {
				if ( strpos( $shortcode, 'sticky="1"' ) )
					return $shortcode;
			}
		}

		if ( $single )
			return array_shift( $shortcodes );
		
		return $shortcodes;
	}
}