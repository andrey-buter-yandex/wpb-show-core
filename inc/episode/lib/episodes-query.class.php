<?php

/**
 * Episode logic class
 * 
 * Use only on module 'episode-stats'
 */
class WPBC_Episodes_Query 
{
	/**
	 * Get episodes by 
	 * @param  string $get_by [description]
	 * @return [type]         [description]
	 */
	public static function get_posts_by( $field, $value, $return = ARRAY_N )
	{
		$value = absint( $value );

		$episodes = '';

		switch ( $field ) {
			case 'show':
				$episodes = self::get_by_show( $value, $return );
				break;
			
			case 'podcast':
				$episodes = self::get_by_podcast( $value, $return );
				break;
		}

		return $episodes;
	}

	/**
	 * Get array of episodes connectd to passed show
	 * @param  int $show_id    Show ID
	 * @param  string $format  Return format ARRAY_N|OBJECT
	 * @return array|object|null
	 */
	protected static function get_by_show( $show_id = 0, $output = ARRAY_N ) 
	{
		if ( !$show_id )
			return;

		// Get show podcast
		$podcast = wp_get_object_terms( $show_id, 'podcast', array( 'fields' => 'ids' ) );

		if ( is_wp_error( $podcast ) )
			return;

		if ( empty( $podcast ) )
			return;

		$podcast = array_shift( $podcast );

		return self::get_by_podcast( absint( $podcast ), $output );
	}

	/**
	 * Get array of episodes connected to the post
	 * @param  integer $podcast_id Podcast term_id
	 * @param  constant  $output   Output format
	 * @return array|object|null
	 */
	protected static function get_by_podcast( $podcast_id = 0, $output = ARRAY_N ) 
	{
		if ( !$podcast_id )
			return;

		$term = get_term( $podcast_id, 'podcast' );

		if ( is_wp_error( $term ) )
			return;

		if ( empty( $term ) )
			return;

		$args = array(
			'post_type'      => 'episode',
			'posts_per_page' => -1,
			'tax_query'      => array(
				array(
					'taxonomy' => 'podcast',
					'field'    => 'id',
					'terms'    => $term->term_id
				)
			)
		);

		// Get all episdodes from this podcast
		if ( OBJECT == $output )
			$episodes = new WP_Query( $args );
		else
			$episodes = get_posts( $args );

		return $episodes;
	}
}