<?php 

/**
 * WPBC_Admin_Single_Episode
 */
class WPBC_Admin_Single_Episode
{
	function load()
	{
		// Automatically set default episode attributes
		add_filter( 'wp_insert_post_data', array( __CLASS__, 'auto_setup_post_title' ), 10, 2 );

		// Insert Enclosures
		add_action( 'post_updated', array( __CLASS__, 'episode_audio_do_enclose' ) );
	}

	/**
	 * Try to automatically setupt episode title
	 * 
	 * @param  array $data     current form data
	 * @param  array $postarr  current post array data
	 * @return array filtered form data array
	 */
	function auto_setup_post_title( $data, $postarr ) 
	{
		if ( 'episode' != $data['post_type'] )
			return $data;

		if ( !empty( $data['post_title'] ) )
			return $data;

		// Set default episode title

		$episodes_in_podcast = 0;

		// First part of the title is podcast slug
		if ( isset( $postarr['tax_input'] ) AND isset( $postarr['tax_input']['podcast'] ) ) {
			foreach ( $postarr['tax_input']['podcast'] as $key => $term_id ) {
				if ( $term_id !== "0" ) {

					$term = get_term_by( 'id', $term_id, 'podcast' );
					$data['post_title'] = $term->slug . '-';

					if ( $episodes_ids = get_objects_in_term( $term_id, 'podcast', array( 'fields' => 'ids' ) ) )
						$episodes_in_podcast = count( $episodes_in_podcast );

					break;
				}
			}
		}

		// Add episode publish date
		$data['post_title'] .= mysql2date( 'mdy', $data['post_date'] );

		// Add episode num
		$data['post_title'] .= '-' . $episodes_in_podcast;

		return $data;
	}

	/**
	 * Add enclosures to episodes
	 * @param  int $post_id   Episode ID
	 * @return void
	 */
	function episode_audio_do_enclose( $post_id ) 
	{
		$post = get_post( $post_id );

		if ( 'episode' != $post->post_type )
			return;

		$media = get_post_meta( $post_id, 'episode_audio', true );

		if ( !$media )
			return;

		// Update episode audio duration with the help of getid3 lib			

		// We can retrieve data for audio that are only on current server
		if ( file_exists( str_replace( home_url(), ABSPATH, $media ) ) ) {

			$getid3_path = WPBC_MODULE_ABS .'/getid3/getid3/getid3.php';

			if ( file_exists( $getid3_path ) ) {
				require_once( $getid3_path );

				$getID3 = new getID3;

				try {
					$fileinfo = $getID3->analyze( str_replace( home_url(), ABSPATH, $media ) );

					if ( isset( $fileinfo['playtime_string'] ) )
						update_post_meta( $post_id, 'episode_audio_duration', $fileinfo['playtime_string'] );

				} catch ( Exception $e ) {
					// Do nothing				
				}
			}	
		}

		// Create html link to connected media file
		$content = sprintf( '%s<a href="%s">%s</a>', $post->post_content, $media, $post->post_title );

		do_enclose( $content, $post_id );
	}
}

if ( is_admin() )
	WPBC_Admin_Single_Episode::load();