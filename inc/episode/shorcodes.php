<?php 

/**
 * Render single episode audio [episode]
 *
 * @uses $theme_episode global object
 * @param array $atts passed shortcode attributes
 * @return string shortcode html
 */

if ( !shortcode_exists( 'episode' ) )
	add_shortcode( 'episode', 'wpbc_episode_shortcode' );

function wpbc_episode_shortcode( $atts = array() ) {
	$atts = shortcode_atts( array(
		'id'       => 0,
		'autoplay' => 0
	), $atts );

	return wpbc_get_episode_content( $atts );
}


/**
 * Render list of show episodes [episodes]
 *
 * @uses $theme_episode global object
 * @param array $atts passed shortcode attributes
 * @return string shortcode html
 */
if ( !shortcode_exists( 'podcast' ) )
	add_shortcode( 'podcast', 'wpbc_podcast_shortcode' );

function wpbc_podcast_shortcode( $atts = array() ) {
	$default = array(
		'term_id' => 0,
		'title'   => __( 'Podcasts' )
	);

	extract( wp_parse_args( $atts, $default ) );

	$term_id = absint( $term_id );

	if ( empty( $term_id ) AND is_singular( 'show' ) )
		$section_query = wpbc_get_episodes_by( 'show', get_the_ID(), OBJECT );
	elseif ( !empty( $term_id ) )
		$section_query = wpbc_get_episodes_by( 'podcast', $term_id, OBJECT );
	else
		return;

	// Add Section title to WP_Query
	$section_query->set( 'section-title', $title );

	$GLOBALS['section_query'] = $section_query;

	ob_start();
	get_theme_part( 'section', 'posts' );
	unset( $GLOBALS['section_query'] );

	return ob_get_clean();
}