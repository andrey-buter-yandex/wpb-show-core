<?php
/**
 * Register show post type
 */

register_post_type( 'episode',
	array(
		'labels' => array(
			'name'               => __( 'Audio Episodes', 'itmwp2' ),
			'singular_name'      => __( 'Audio Episode', 'itmwp2' ),
			'add_new'            => __( 'Add Audio Episode', 'itmwp2' ),
			'add_new_item'       => __( 'Add New Audio Episode', 'itmwp2' ),
			'edit'               => __( 'Edit', 'itmwp2' ),
			'edit_item'          => __( 'Edit Audio Episode', 'itmwp2' ),
			'new_item'           => __( 'New Audio Episode', 'itmwp2' ),
			'view'               => __( 'View Audio Episodes', 'itmwp2' ),
			'view_item'          => __( 'View Audio Episode', 'itmwp2' ),
			'search_items'       => __( 'Search Audio Episodes', 'itmwp2' ),
			'not_found'          => __( 'Not found', 'itmwp2' ),
			'not_found_in_trash' => __( 'No Audio Episodes found in Trash', 'itmwp2' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => true,
		'taxonomies'    => array( 
			'podcast', 
			'collection' 
		),
		'has_archive'   => true,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			'editor', 
			'thumbnail' 
		),
		'rewrite'       => array(
			'with_front' => false
		),
		'can_export'    => true,
	)
);