<?php 

/**
 * Handle every member aspect
 */

add_filter( 'wpbc_populate_meta_boxes', 'wpbc_populate_episode_meta_boxes' );
function wpbc_populate_episode_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'episode';
	$prefix = $post_type .'_';
	$meta_boxes[] = array(
		'id'       => $prefix .'meta',
		'title'    => __( 'Attributes', 'itmwp' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'label' => 'Audio',
				'id'    => $prefix . 'audio',
				'type'  => 'upload',
				'desc'  => 'Upload episode audio file'
			),
			array(
				'label'    => 'Preroll',
				'id'       => $prefix . 'preroll',
				'type'     => 'custom_post_type',
				'desc'     => 'Select episode preroll',
				'settings' => array(
					'post_type'   => 'preroll',
					'type'        => 'select',
					'same_tax'    => 'podcast',
					'select_post' => __( 'Select preroll', 'itmwp2' ),
					'no_terms'    => __( 'Episode must be attached to podcast.', 'itmwp2' ),
					'no_posts'    => __( 'No prerolls found attached to this podcast.', 'itmwp2' )
				)
			),
			array(
				'label' => 'Itunes Subtitle',
				'id'    => $prefix . 'itunes_subtitle',
				'type'  => 'text',
				'desc'  => 'Subtitle used in podcast feed for this episode.'
			),
		)
	);

	return $meta_boxes;
}