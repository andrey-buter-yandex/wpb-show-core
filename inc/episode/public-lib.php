<?php

/* 
 * DEPENDS ON class WPBC_Get_Episode_Content
 *
=================================================================*/

/**
 * ==============================
 * content.php
 * front\slider.php
 * widget\recent-gallery.php
 * widget\recent-summary.php
 * widget\recent-thumbnail-content-first.php
 * widget\recent-thumbnail-summary.php
 * widget\recent-thumbnail.php
 * widget\recent.php
 *
 * depends on class WPBC_Get_Episode_Content()
 */
function wpbc_the_post_episode() {
	echo wpbc_get_post_episode( get_the_ID() );
}

/**
 * 
 * ==============================
 *
 * depends on class WPBC_Get_Episode_Content
 *
 */
function wpbc_get_post_episode( $post_id = null ) {
	return WPBC_Get_Episode_Content::get_post_episode( $post_id );
}

/**
 * 
 * ==============================
 *
 * depends on class WPBC_Get_Episode_Content
 *
 */
function wpbc_get_episode_content( $atts = array() ) {
	return WPBC_Get_Episode_Content::get_content( $atts );
}


/* 
 * DEPENDS ON class WPBC_Episodes_Query
 *
=================================================================*/


/**
 * Get episodes by 
 * @param  string $get_by [description]
 * @return [type]         [description]
 *
 * ==============================
 *
 * depends on class WPBC_Episodes_Query
 *
 * ===============================
 * inc\module\episode-stats\class\class-player-stats-list-table.php
 * inc\module\episode-stats\class\class-podcast-stats-list-table.php
 */
function wpbc_get_episodes_by( $field, $value, $return = ARRAY_N ) {
	return WPBC_Episodes_Query::get_posts_by( $field, $value, $return );
}